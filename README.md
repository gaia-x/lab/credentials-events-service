# Credentials Event Service

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Usage

This API allows to share either gaia-x compliance verifiable credentials, or custom VCs.
Those custom VCs can only contain a reference to the original VC ID and no other data.

### Examples

#### Gaia-X compliance credential event
This API implements both Tagus and Loire(v2) Gaia-X compliance versions which each support different VC formats

##### Tagus

```json
{
  "specversion": "1.0",
  "type": "eu.gaia-x.credential",
  "source": "/mycontext",
  "subject": null,
  "id": null,
  "time": "2018-04-05T17:31:00Z",
  "datacontenttype": "application/json",
  "data": {
    "@context": [
      "https://www.w3.org/2018/credentials/v1",
      "https://w3id.org/security/suites/jws-2020/v1",
      "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "type": [
      "VerifiableCredential"
    ],
    "id": "https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203",
    "issuer": "did:web:compliance.lab.gaia-x.eu:development",
    "issuanceDate": "2023-07-26T13:05:36.310Z",
    "expirationDate": "2023-10-24T13:05:36.310Z",
    "credentialSubject": [
      {
        "type": "gx:compliance",
        "id": "https://gaia-x.eu/legalRegistrationNumberVC.json",
        "integrity": "sha256-03d826422b47bf9e31c541d1c26a2a26ae62e579430f5e570c99418645302d1f",
        "version": "22.10"
      },
      {
        "type": "gx:compliance",
        "id": "https://wizard.lab.gaia-x.eu/api/credentials/2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8DFPWTYo5iRYkn8kvgU3AjMXc2qTbhuMHCpucKGgT1ZMkcHUygZkt11iD3T8VJNKYwsdk4MGoZwdqoFUuTKVcsXVTBA4ofD1Dtqzjavyng5WUpvJf4gRyfGkMvYYuHCgay8TK8Dayt6Rhcs3r2d1gRCg2UV419S9CpWZGwKQNEXdYbaB2eTiNbQ83KMd4mj1oSJgF7LLDZLJtKJbhwLzR3x35QUqEGevRxnRDKoPdHrEZN7r9TVAmvr9rt7Xq8eB4zGMTza59hisEAUaHsmWQNaVDorqFyZgN5bXswMK1irVQ5SVR9osCCRrKUKkntxfakjmSqapPfveMP39vkgTXfEhsfLUZXGwFcpgLpWxWRn1QLnJY11BVymS7DyaSvbSKotNFQxyV6vghfM2Jetw1mLxU5qsQqDYnDYJjPZQSmkwxjX3yenPVCz6N2ox83tj9AuuQrzg5p2iukNdunDd2QCsHaMEtTq9JVLzXtWs2eZbPkxCBEQwoKTGGVhKu5yxZjCtQGc#9894e9b0a38aa105b50bb9f4e7d0975641273416e70f166f4bd9fd1b00dfe81d",
        "integrity": "sha256-6d68f0f705a3b6c35cfa0540a0549fade845ffcf2f6a7538f1756880cc54ea8e",
        "version": "22.10"
      },
      {
        "type": "gx:compliance",
        "id": "https://bakeup.io/tandcs.json",
        "integrity": "sha256-924a0f5691a5a99521e5556b30b070de6b08bc5cd8495f727861da68712c9401",
        "version": "22.10"
      }
    ],
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2023-07-26T13:05:36.983Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..GCsZsFkV7C7z7Ma1a36Bf4jlwPrx7tJsbzsqhwg99tGesamyA0ThO2laKef3z_86-A7tHcuzWgmzFjacUZFJwSOxCggrT5NKz7HBQO2Ok3PDHL1LEwDKoN4ffJQVOHomzdT1eFp3tRwm6JtX5hGqXLOTfQ36L1Myrk3CbPaeefKyk7D6co8g668_PPWV7IQNez7GihXJYkzYI07vm_gkh_z1KMrwG8FcPtogFOK3iYuOUuYyT06Az9DYjowpvTR526gFgmYN4hR21yv5PW78HLKbAX8x8Mfh1Nl5ozxTjkShFghXudE6fS0N8RkbFjgo-3h3rLg7Ac2OAOExkkyxAw",
      "verificationMethod": "did:web:compliance.lab.gaia-x.eu:development"
    }
  }
}
```

##### Loire

```json
{
  "specversion": "1.0",
  "type": "eu.gaia-x.credential",
  "source": "/mycontext",
  "subject": null,
  "id": null,
  "time": "2018-04-05T17:31:00Z",
  "datacontenttype": "application/json",
  "data_base64": "eyJhbGciOiJQUzI1NiIsImlzcyI6ImRpZDp3ZWI6Y29tcGxpYW5jZS5sYWIuZ2FpYS14LmV1Om1haW4iLCJraWQiOiJkaWQ6d2ViOmNvbXBsaWFuY2UubGFiLmdhaWEteC5ldTptYWluI1g1MDktSldLIiwiaWF0IjoxNzM0NjcwNDM5ODExLCJleHAiOjE3NDI0NDY0Mzk4MDAsImN0eSI6InZjIiwidHlwIjoidmMtbGQrand0In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsImd4OkxhYmVsQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vc3RvcmFnZS5nYWlhLXguZXUvY3JlZGVudGlhbC1vZmZlcnMvYjNlMGEwNjgtNGJmOC00Nzk2LTkzMmUtMmZhODMwNDNlMjAzIiwiaXNzdWVyIjoiZGlkOndlYjpjb21wbGlhbmNlLmxhYi5nYWlhLXguZXU6bWFpbiIsInZhbGlkRnJvbSI6IjIwMjQtMTItMjBUMDQ6NTM6NTkuODExWiIsInZhbGlkVW50aWwiOiIyMDI1LTAzLTIwVDA0OjUzOjU5LjgwMFoiLCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6Imh0dHBzOi8vc3RvcmFnZS5nYWlhLXguZXUvY3JlZGVudGlhbC1vZmZlcnMvYjNlMGEwNjgtNGJmOC00Nzk2LTkzMmUtMmZhODMwNDNlMjAzI2NzIiwiZ3g6bGFiZWxMZXZlbCI6IlNDIiwiZ3g6ZW5naW5lVmVyc2lvbiI6IjIuNy4xIiwiZ3g6cnVsZXNWZXJzaW9uIjoiQ0QyNC4wNiIsImd4OmNvbXBsaWFudENyZWRlbnRpYWxzIjpbeyJpZCI6Imh0dHBzOi8vMjZub3YyMDI0LmRldi5zbWFydC14LnNtYXJ0c2Vuc2VsYWJzLmNvbS8ud2VsbC1rbm93bi9wYXJ0aWNpcGFudC5qc29uIiwidHlwZSI6IkludGVybmV0U2VydmljZVByb3ZpZGVyIiwiZ3g6ZGlnZXN0U1JJIjoic2hhMjU2LTMxYTU4YTQxN2ViMmM5ZmI5OWZkYzI5NTUzZDQxZGEwMTFmY2I2ZWUyNmFmMGYwOTU1OTQ3MThiMWE2ZWVlOWYifSx7ImlkIjoiaHR0cHM6Ly8yNm5vdjIwMjQuZGV2LnNtYXJ0LXguc21hcnRzZW5zZWxhYnMuY29tLy53ZWxsLWtub3duL2lzc3Vlci5qc29uIiwidHlwZSI6Imd4Oklzc3VlciIsImd4OmRpZ2VzdFNSSSI6InNoYTI1Ni1kZjZlY2JjNmVmOWJjNmVhZmIwNzdiZDUzNzg2OWFkNjRiYzBhYmU5MWI5OGU1YjQwM2I3NTY3NTE1Y2I3YmVmIn0seyJpZCI6Imh0dHBzOi8vMjZub3YyMDI0LmRldi5zbWFydC14LnNtYXJ0c2Vuc2VsYWJzLmNvbS8ud2VsbC1rbm93bi9scm4uanNvbiIsInR5cGUiOiJneDpWYXRJRCIsImd4OmRpZ2VzdFNSSSI6InNoYTI1Ni1iODA4NjMxMTdiZWE5MGUwZTE5NjAyZDdkNThlOTY5ZmRkNWRmZjYzNTNkYTE2MWU4ZjUxZjk1NzdhNjY5YTYxIn0seyJpZCI6Imh0dHBzOi8vMjZub3YyMDI0LmRldi5zbWFydC14LnNtYXJ0c2Vuc2VsYWJzLmNvbS8ud2VsbC1rbm93bi9zby5qc29uIiwidHlwZSI6IlNlcnZpY2VPZmZlcmluZyIsImd4OmRpZ2VzdFNSSSI6InNoYTI1Ni1iN2EzYjUxMTg3N2M5NDdlNDA1ZjZlODMxNDYyODMxOTRjOGMxMTc2ZGE3YmU5ZmFiYmFmODgwOTAwY2I1MzBlIn0seyJpZCI6Imh0dHBzOi8vMjZub3YyMDI0LmRldi5zbWFydC14LnNtYXJ0c2Vuc2VsYWJzLmNvbS8ud2VsbC1rbm93bi9wci5qc29uIiwidHlwZSI6IlBoeXNpY2FsUmVzb3VyY2UiLCJneDpkaWdlc3RTUkkiOiJzaGEyNTYtN2VmMWRlN2FiYTc4NzRiZmYyNjIzNmNhY2IzY2NiMjBjZmQ2MTc4NTA1MWM4OWY3YWNhYjNjMzAyOWFiNjYxYSJ9LHsiaWQiOiJodHRwczovLzI2bm92MjAyNC5kZXYuc21hcnQteC5zbWFydHNlbnNlbGFicy5jb20vLndlbGwta25vd24vZHIuanNvbiIsInR5cGUiOiJEYXRhY2VudGVyIiwiZ3g6ZGlnZXN0U1JJIjoic2hhMjU2LWUyYjE5MWYyOTg4ODUzMGRmMTU1N2Q3ZmNmOWEwMTgyY2YzOTA4MzYzZWY3MmExYjcwOWI3OWQ4ODk0YWUwY2MifSx7ImlkIjoiaHR0cHM6Ly8yNm5vdjIwMjQuZGV2LnNtYXJ0LXguc21hcnRzZW5zZWxhYnMuY29tLy53ZWxsLWtub3duL3BvcC5qc29uIiwidHlwZSI6IlBvaW50T2ZQcmVzZW5jZSIsImd4OmRpZ2VzdFNSSSI6InNoYTI1Ni0yNzA1YzQwZTYyZDU2MzhkNjdlYzZjMTAzNDgzZWQwNDYyOTdmNGY3Y2M2NGE4MzVmMzE4YjMwY2VhOWFkZjA4In0seyJpZCI6Imh0dHBzOi8vMjZub3YyMDI0LmRldi5zbWFydC14LnNtYXJ0c2Vuc2VsYWJzLmNvbS8ud2VsbC1rbm93bi9pcGkuanNvbiIsInR5cGUiOiJJbnRlcmNvbm5lY3Rpb25Qb2ludElkZW50aWZpZXIiLCJneDpkaWdlc3RTUkkiOiJzaGEyNTYtNmRkNjZmMDRlYTU5ZWQwODBjZWE5MDA2NDY2MjYyNGY2NzEwNjRmNDlmYzQyZjAwNzU1ZWIzYWNhYjlmNTRjNyJ9LHsiaWQiOiJodHRwczovLzI2bm92MjAyNC5kZXYuc21hcnQteC5zbWFydHNlbnNlbGFicy5jb20vLndlbGwta25vd24vci5qc29uIiwidHlwZSI6IkF2YWlsYWJpbGl0eVpvbmUiLCJneDpkaWdlc3RTUkkiOiJzaGEyNTYtOGM1YmExYjFjMDFlZTZhZWI3OGM3Njc3ZmI3YWNiMjlhODhlMDZlNTgxOTk0MTc1MzA2Yjc0YWEyODQzN2E0NiJ9XSwiZ3g6dmFsaWRhdGVkQ3JpdGVyaWEiOlsiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMi41IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMS4xIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMi4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMi4zIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMS4zIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDIuMS4zIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMi44IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDIuMS4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS44IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDIuMi4xIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDIuMi4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS43IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDEuMy4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDIuMi43IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS41IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4xIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS40IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4zIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS42IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS45IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDQuMS4xIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDQuMS4yIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4xMSIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AzLjEuMTMiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9zcGVjcy9jZDI0LjA2L2NyaXRlcmlvbi9QMy4xLjE0IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4xNSIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AzLjEuMTYiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9zcGVjcy9jZDI0LjA2L2NyaXRlcmlvbi9QMy4xLjE3IiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4xOCIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AzLjEuMTkiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9zcGVjcy9jZDI0LjA2L2NyaXRlcmlvbi9QMy4xLjIwIiwiaHR0cHM6Ly93M2lkLm9yZy9nYWlhLXgvc3BlY3MvY2QyNC4wNi9jcml0ZXJpb24vUDMuMS4xMiIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1A1LjIuMSIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AxLjIuNiIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AxLjIuNyIsImh0dHBzOi8vdzNpZC5vcmcvZ2FpYS14L3NwZWNzL2NkMjQuMDYvY3JpdGVyaW9uL1AzLjEuMTAiXX19.WouvUPImNzzJf5AbnI3DHlwMQLJrk5DkWbBkhV-LkHIEUbmnP8vlSA-MkKlPsS2drDBdIJ7pBxRzf5ppSHHhZbiUwln7D1Ey98PhpomtWn7wch571KCj2avLK_np1nOU4rVeu9qftyyi-i6FeYpZepP-K37Hn-x8nQr5oxvzO5yc7V_roObqx9XeRBwKvHZWDdJOTDUy0I9PgYMNJI5ZwtNOWkFrBNzMovlrJIK58YK0Y9z7d7e74CejEadA2-LCD8Qnyk1PhNWNDJ4Vm9gG1k9BYm-iz5Wq-Fk4nA8JLtLwIb7w9JIuNYpXfLuEm80Rh5MSUN2yZtuwB-lXIixi9g"
}
```

#### Non-Gaia-X credential event

```json
{
  "specversion": "1.0",
  "type": "com.mycompany.event",
  "source": "",
  "subject": null,
  "id": null,
  "time": "2024-02-05T10:31:00Z",
  "datacontenttype": "application/json",
  "data": {
    "@context": [
      "https://www.w3.org/2018/credentials/v1",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "type": [
      "VerifiableCredential"
    ],
    "id": "https://mycompany.com/my-custom-VC.json",
    "issuer": "did:web:mycompany.com",
    "issuanceDate": "2024-02-05T10:05:36.310Z",
    "expirationDate": "2025-02-05T10:05:36.310Z",
    "credentialSubject": {
      "id": "https://mycompany.com/my-custom-VC.json"
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2024-02-05T10:05:36.310Z",
      "proofPurpose": "assertionMethod",
      "jws": "...",
      "verificationMethod": "did:web:mycompany.com#JWS-2020"
    }
  }
}
```

## Requirements

- Java 17
- Docker

## Running the application in dev mode

### Start application

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Environment variables

| env variable               | targeted property                 | default value                                                    | usage                                 |
|----------------------------|-----------------------------------|------------------------------------------------------------------|---------------------------------------|
| trustedIssuersListUrl      | app.trusted-issuers-list-url      | https://registry.lab.gaia-x.eu/v1/api/trusted-issuers/compliance | url to fetch trusted issuers list     |
| trustedIssuersListDisabled | app.trusted-issuers-list-disabled | false                                                            | disable trusted issuers check         |
| universalDidResolverUrl    | app.universal-did-resolver-url    | https://dev.uniresolver.io/1.0/                                  | URL of the universal DID resolver API |
| dbHost                     | dbHost                            |                                                                  | Hostname of the database server       |
| dbPort                     | dbPort                            |                                                                  | Port of the database server           |
| dbName                     | dbName                            |                                                                  | Database name on the database server  |
| dbUser                     | dbUser                            |                                                                  | Username for the database server      |
| dbPassword                 | dbPassword                        |                                                                  | Password for the database server      |

## Packaging and running the application

The application can be packaged using:

```shell script
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/credentials-events-service-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- Hibernate ORM with Panache and Kotlin ([guide](https://quarkus.io/guides/hibernate-orm-panache-kotlin)): Define your
  persistent model in Hibernate ORM with Panache
- Kotlin ([guide](https://quarkus.io/guides/kotlin)): Write your services in Kotlin
