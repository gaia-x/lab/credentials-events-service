# Credentials Events Service Implementation Guide

[TOC]

## Introduction

The Credentials Events Service (CES) is a software component that stores Gaia-X Compliant Verifiable Credentials and shares them.

CES enables catalogues federation by storing Gaia-X Compliant Verifiable Credentials and making them accessible to everyone.

The CES stores and shares data using [Cloud Events specifications](https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#required-attributes)


## How to work with Credentials Events Service

### Fetch Credentials Events from the CES

1. **Fetch VCs**: Utilize the [**CES API**](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/) to retrieve the **Credentials Events**. The **pagination** feature provided by the CES helps to manage large data retrievals. No authentication is required.

We offer a mechanism allowing you to retrieve the latest added credentials since your last call by using a query parameter named `lastReceivedID`. You can provide the id of the last event you received and submit it in your subsequent requests.

```json
{
  "specversion": "1.0",
  "type": "eu.gaia-x.credential",
  "source": "/mycontext",
  "subject": null,
  "id": "6889dd2a-85f5-4357-92b6-1da1feddb8c3", // Store this id
  "time": "2018-04-05T17:31:00Z",
  "cesreceiveddate": "2023-09-14T14:04:32.160719Z",
  "datacontenttype": "application/json",
  "data": {...} // Compliance VC
}
```

>GET response example:
>
>https://ces-v1.lab.gaia-x.eu/credentials-events?page=0&size=5
>
> ---
>GET response example with lastReceivedID param:
>
>https://ces-v1.lab.gaia-x.eu/credentials-events?page=0&size=5&lastReceivedID=b3e0a068-4bf8-4796-932e-2fa83043e203

2. **Treat Compliance VCs**, you can:
   - Resolve the Compliance VC id to retrieve the original VC
   - Compute VC integrity and compare it to the hash provided to ensure data were not tampered
   - Filter the VCs based on your catalog needs

3. **Integrate Directly into Your Catalog**: As there's no need to transform the data, directly integrate the fetched VC into your catalog. This ensures that the VCs maintain their original integrity and accuracy as intended by the initial providers.

> __IMPORTANT NOTE__ Catalogs are responsible to handle properly newer credential being submitted with the same ID. That means original provider may want to update the original VC. They can either reject changes like that, or use proper versionning in their system, or even just replace the credential in the catalog.

### Store your Verifiable Credentials in the Credentials Events Service

To push your Verifiable Credentials in the Credentials Events Service (CES), follow the steps below:

1. Create and sign your **Verifiable Credentials** and get your **Compliance VC**. (See [Compliance API guide](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/README-api.md?ref_type=heads#get-started-using-the-api))

2. **Post to CES**: Upon receiving a **Compliance VC** from the **Compliance API**, the final step is to post this Compliance VC to the **Credentials Events Service** using [**Cloud Events format**](https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#required-attributes).

3. Check your credentials by using the *Location Header* returned by the CES on your post request

By doing so, your Verifiable Credentials gets **stored** and are made accessible to other catalogs that are using the CES.

#### Post to CES example:

```json
{
  "specversion": "1.0", // Mandatory
  "type": "eu.gaia-x.credential", // Mandatory
  "source": "/mycontext", // Can be whatever you want (catalog, company, etc..)
  "time": "2018-04-05T17:31:00Z", // ISO-8202 - ZonedDateTime
  "datacontenttype": "application/json", // JSON only for now
  "data": {...} // Your Gaia-X Compliance credential
}
```

POST to: https://ces-v1.lab.gaia-x.eu/credentials-events


**Location and UUID** are in CES **response headers**

```
   content-length: 0
   date: Tue,10 Oct 2023 15:41:47 GMT
   location: https://ces-development.lab.gaia-x.eu/credentials-events/1b6bab1e-d371-401b-91d9-5c9bee86e74b
   strict-transport-security: max-age=15724800; includeSubDomains
```


>For those unfamiliar with this process, the [**Gaia-X Wizard**](https://wizard.lab.gaia-x.eu/) can be used to learn the creation and submission of VCs. (See User Guide tab)

## Architecture

**Credentials Services API**: This [API](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/) consists of three distinct endpoints to retrieve and post **Credentials Events**

**Database**: We rely on a volatile database in order to be able to get **paginated Credentials Events** and move cursor around data


```mermaid
 graph TB
   ces[Credentials Events Service API]
   catalogue1[Catalog 1]
   catalogue2[Catalog 2]
   database[Database]

   ces -->|HTTP GET| catalogue1
   catalogue1 -->|HTTP POST| ces
   ces -->|HTTP GET| catalogue2
   catalogue2 -->|HTTP POST| ces
   database <-->|Events| ces
```

## API

The CES API is available at [CES API Swagger](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/)

A development version of the CES API is available at [CES API Swagger](https://ces-development.lab.gaia-x.eu/q/swagger-ui/)

This development version allows you to submit credentials validated by the development compliance engine


## Useful links
- [CES API Swagger](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/)
- [Compliance API documentation](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/README-api.md?ref_type=heads)
- [Gaia-X Wizard](https://wizard.lab.gaia-x.eu/)
- [Cloud Events format](https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#required-attributes)

