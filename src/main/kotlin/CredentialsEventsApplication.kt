import io.quarkus.runtime.Application
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.info.Contact
import org.eclipse.microprofile.openapi.annotations.info.Info
import org.eclipse.microprofile.openapi.annotations.info.License

@OpenAPIDefinition(
    info = Info(
        title = "Credentials Events Service",
        description = "This service allows you to send Gaia-X compliance credential to share them to the world.",
        termsOfService = "", contact = Contact(name = "Gaia-X AISBL CTO Team", url = "", email = ""),
        license = License(name = "EPL2", url = "https://www.eclipse.org/legal/epl-2.0/"), version = ""
    )
)
abstract class CredentialsEventsApplication : Application(false)