package eu.gaiax.ces.common.service

import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.Produces
import org.eclipse.microprofile.config.inject.ConfigProperty
import uniresolver.UniResolver
import uniresolver.client.ClientUniResolver
import java.net.URI

@ApplicationScoped
class DidResolverProducer(
    @ConfigProperty(name = "app.universal-did-resolver-url")
    val universalDidResolverUrl: String
) {

    @Produces
    @ApplicationScoped
    fun uniResolver(): UniResolver {
        return ClientUniResolver.create(URI.create(universalDidResolverUrl))
    }

}