package eu.gaiax.ces.common.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import eu.gaiax.ces.common.exception.InvalidIssuerException
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.LocalDateTime

@ApplicationScoped
class TrustedIssuersService(
    @ConfigProperty(name = "app.trusted-issuers-list-url") val trustedIssuerListURL: String,
    @ConfigProperty(
        name = "app.trusted-issuers-list-disabled",
        defaultValue = "false"
    ) val trustedIssuersListDisabled: Boolean
) {
    private var issuers: List<String> = ArrayList()
    private var lastFetchTime: LocalDateTime = LocalDateTime.MIN

    /**
     * Whether an issuer is allowed to emit compliance credentials
     * The check can be disabled via `trustedIssuersListDisabled` environment flag<
     * @return true if the issuer is allowed, false otherwise.
     */
    fun isIssuerAllowed(issuer: String): Boolean {
        if (trustedIssuersListDisabled) {
            return true
        }
        val issuerFormatted = prepareDIDForResolution(issuer)
        return this.getIssuers().indexOf(issuerFormatted) > -1
    }

    /**
     * Returns the trusted issuers list. This list is cached for 30 minutes
     * @return the list of trusted issuers. If cached for more than 30 minutes, the list is fetched again before returning
     */
    fun getIssuers(): List<String> {
        if (lastFetchTime.plusMinutes(30).isBefore(LocalDateTime.now())) {
            val request = HttpRequest.newBuilder().GET().uri(URI.create(trustedIssuerListURL)).build()
            this.issuers = getIssuersFromRequest(request)
        }
        return this.issuers
    }

    private fun getIssuersFromRequest(request: HttpRequest): List<String> {
        try {
            val response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString())
            if (response.statusCode() == 200) {
                return jacksonObjectMapper().readValue(response.body())
            }
        } catch (e: Exception) {
            throw InvalidIssuerException("Unable to retrieve trusted issuers list")
        }
        throw InvalidIssuerException("Unable to retrieve trusted issuers list")
    }


    companion object {
        fun prepareDIDForResolution(issuer: String): String {
            return issuer.replace(Regex("did:[a-zA-Z0-9]*:"), "")
                .replace(":", "/")
        }
    }
}