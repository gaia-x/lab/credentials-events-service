package eu.gaiax.ces.common.exception

class ExpiredCredentialException(message: String) : RuntimeException(message)