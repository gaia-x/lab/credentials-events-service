package eu.gaiax.ces.common.exception

class InvalidContentException(message:String) : Exception(message)