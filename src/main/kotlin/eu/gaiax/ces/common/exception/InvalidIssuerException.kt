package eu.gaiax.ces.common.exception

class InvalidIssuerException(message: String) : Exception(message)