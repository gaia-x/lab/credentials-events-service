package eu.gaiax.ces.common.exception

class InvalidSignatureException(message: String) : RuntimeException(message)