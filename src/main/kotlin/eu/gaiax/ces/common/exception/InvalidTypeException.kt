package eu.gaiax.ces.common.exception

class InvalidTypeException(message:String) : Exception(message)