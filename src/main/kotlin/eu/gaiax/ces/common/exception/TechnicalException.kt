package eu.gaiax.ces.common.exception

class TechnicalException(message:String) : Exception(message)