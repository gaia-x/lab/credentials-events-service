package eu.gaiax.ces.common.providers

import eu.gaiax.ces.common.exception.InvalidTypeException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.BAD_REQUEST
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class InvalidTypeExceptionMapper : ExceptionMapper<InvalidTypeException> {
    override fun toResponse(exception: InvalidTypeException): Response {
        return Response.status(BAD_REQUEST)
            .entity(ErrorMessage(exception.message!!)).build()
    }
}