package eu.gaiax.ces.common.providers

import com.fasterxml.jackson.databind.exc.MismatchedInputException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.BAD_REQUEST
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class MismatchedInputExceptionMapper : ExceptionMapper<MismatchedInputException> {
    override fun toResponse(exception: MismatchedInputException): Response {
        return Response.status(BAD_REQUEST)
            .entity(ErrorMessage("At least one field is missing in the event ${exception.path.map { it.fieldName }}"))
            .build()
    }
}