package eu.gaiax.ces.common.providers

import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class OtherExceptionMapper : ExceptionMapper<Exception> {
    override fun toResponse(exception: Exception): Response {
        return Response.status(INTERNAL_SERVER_ERROR)
            .entity(ErrorMessage("An error occurred on the server side ${exception.message}"))
            .build()
    }
}