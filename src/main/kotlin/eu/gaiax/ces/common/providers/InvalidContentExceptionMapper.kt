package eu.gaiax.ces.common.providers

import eu.gaiax.ces.common.exception.InvalidContentException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.BAD_REQUEST
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class InvalidContentExceptionMapper : ExceptionMapper<InvalidContentException> {
    override fun toResponse(exception: InvalidContentException): Response {
        return Response.status(BAD_REQUEST)
            .entity(ErrorMessage(exception.message!!)).build()
    }
}