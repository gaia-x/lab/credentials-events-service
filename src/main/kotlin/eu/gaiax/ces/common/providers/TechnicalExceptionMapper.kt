package eu.gaiax.ces.common.providers

import eu.gaiax.ces.common.exception.TechnicalException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class TechnicalExceptionMapper : ExceptionMapper<TechnicalException> {
    override fun toResponse(exception: TechnicalException): Response {
        return Response.status(INTERNAL_SERVER_ERROR)
            .entity(ErrorMessage(exception.message!!)).build()
    }
}