package eu.gaiax.ces.common.providers

import eu.gaiax.ces.common.exception.InvalidIssuerException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.CONFLICT
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class InvalidIssuerExceptionMapper : ExceptionMapper<InvalidIssuerException> {
    override fun toResponse(exception: InvalidIssuerException): Response {
        return Response.status(CONFLICT)
            .entity(ErrorMessage(exception.message!!)).build()
    }
}