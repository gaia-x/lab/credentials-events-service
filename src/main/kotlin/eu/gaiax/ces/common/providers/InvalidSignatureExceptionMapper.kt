package eu.gaiax.ces.common.providers

import eu.gaiax.ces.common.exception.InvalidSignatureException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status.CONFLICT
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider

@Provider
class InvalidSignatureExceptionMapper : ExceptionMapper<InvalidSignatureException> {
    override fun toResponse(exception: InvalidSignatureException): Response {
        return Response.status(CONFLICT)
            .entity(ErrorMessage(exception.message!!)).build()
    }
}