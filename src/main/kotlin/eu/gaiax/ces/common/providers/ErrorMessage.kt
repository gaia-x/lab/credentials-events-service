package eu.gaiax.ces.common.providers

data class ErrorMessage(val message: String)
