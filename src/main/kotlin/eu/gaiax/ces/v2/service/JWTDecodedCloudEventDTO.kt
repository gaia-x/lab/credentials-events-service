package eu.gaiax.ces.v2.service

import com.fasterxml.jackson.annotation.JsonProperty
import eu.gaiax.ces.v2.entity.JWTEvent
import eu.gaiax.ces.v2.utils.decodeJWT
import org.eclipse.microprofile.openapi.annotations.media.SchemaProperty
import java.time.ZonedDateTime
import java.util.*

data class JWTCloudEventDTO(
    @JsonProperty("specversion") val specVersion: String,
    val type: String,
    val source: String,
    val subject: String?,
    val id: UUID?,
    val time: ZonedDateTime,
    @JsonProperty("datacontenttype") val dataContentType: String,
    @SchemaProperty(
        name = "data_base64",
        description = "A Compliance verifiable credential in VC-JWT format"
    )
    @JsonProperty("data_base64") val dataBase64: String
) {
    companion object {
        fun toDecodedEvent(
            id: UUID,
            creationDate: ZonedDateTime,
            cloudEventDTO: JWTCloudEventDTO
        ): JWTDecodedCloudEventDTO {
            return JWTDecodedCloudEventDTO(
                cloudEventDTO.specVersion,
                cloudEventDTO.type,
                cloudEventDTO.source,
                cloudEventDTO.subject,
                id,
                cloudEventDTO.time,
                cloudEventDTO.dataContentType,
                creationDate,
                cloudEventDTO.dataBase64,
                decodeJWT(cloudEventDTO.dataBase64).jwtClaims.claimsMap
            )
        }

        fun toEvent(verifiableCredentialCloudEvent: JWTCloudEventDTO): JWTEvent {
            val event = JWTEvent()
            event.id = UUID.randomUUID()
            event.creationDate = ZonedDateTime.now()
            event.content = verifiableCredentialCloudEvent
            return event
        }
    }
}

data class JWTDecodedCloudEventDTO(
    @JsonProperty("specversion") val specVersion: String,
    val type: String,
    val source: String,
    val subject: String?,
    val id: UUID?,
    val time: ZonedDateTime,
    @JsonProperty("datacontenttype") val dataContentType: String,
    @JsonProperty("cesreceiveddate") val cesReceivedDate: ZonedDateTime,
    @SchemaProperty(
        name = "data_base64",
        description = "A Compliance verifiable credential in VC-JWT format"
    )
    @JsonProperty("data_base64") val dataBase64: String,
    @SchemaProperty(
        name = "data",
        description = "A verifiable Credential issued by the compliance"
    ) val data: Map<String, Any>
) {}