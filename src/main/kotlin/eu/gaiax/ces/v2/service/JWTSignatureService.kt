package eu.gaiax.ces.v2.service

import eu.gaiax.ces.common.exception.InvalidSignatureException
import eu.gaiax.ces.v2.utils.decodeJWT
import eu.gaiax.ces.v2.utils.getJWTHeader
import foundation.identity.did.DIDDocument
import jakarta.enterprise.context.ApplicationScoped
import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jwk.JsonWebKey
import org.jose4j.jws.AlgorithmIdentifiers
import org.jose4j.jws.JsonWebSignature
import org.jose4j.jwt.consumer.JwtContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uniresolver.UniResolver
import java.net.URI


@ApplicationScoped
class JWTSignatureService(
    val uniResolver: UniResolver
) {
    val logger: Logger = LoggerFactory.getLogger(JWTSignatureService::class.java)

    /**
     * Whether the event signature is valid
     * @return true if the signature is valid, false otherwise
     */
    fun isSignatureValid(token: String): Boolean {
        try {
            val decodedJWT: JwtContext = decodeJWT(token)
            val eventId = decodedJWT.jwtClaims.claimsMap.getOrElse("id") { "unknown-id" }
            logger.info("[event:{}]Validating signature", eventId)

            val verificationMethod = getJWTHeader(decodedJWT, "kid")
            val did = this.resolveDID(getJWTHeader(decodedJWT, "iss")) ?: return false

            val publicKey = this.retrievePublicKey(did, verificationMethod) ?: return false

            logger.info(
                "[event:{}]Validated signature",
                eventId
            )
            return verifySignature(token, publicKey)
        } catch (e: Exception) {
            throw InvalidSignatureException("Unable to validate signature of the payload")
        }
    }

    private fun verifySignature(signature: String, publicKey: JsonWebKey): Boolean {
        val jws = JsonWebSignature()
        jws.compactSerialization = signature
        jws.setAlgorithmConstraints(
            AlgorithmConstraints(
                AlgorithmConstraints.ConstraintType.PERMIT,
                AlgorithmIdentifiers.RSA_USING_SHA256,
                AlgorithmIdentifiers.ECDSA_USING_P256_CURVE_AND_SHA256,
                AlgorithmIdentifiers.RSA_PSS_USING_SHA256
            )
        )
        jws.key = publicKey.key
        return jws.verifySignature()
    }

    private fun resolveDID(did: String): DIDDocument? {
        return try {
            uniResolver.resolve(did).didDocument
        } catch (exception: Exception) {
            this.logger.warn("Unable to resolve did {} - {}", did, exception.message)
            null
        }
    }

    private fun retrievePublicKey(did: DIDDocument, verificationMethod: String): JsonWebKey? {
        val didVerificationMethod = did.allVerificationMethodsAsMap[URI.create(verificationMethod)] ?: return null
        return JsonWebKey.Factory.newJwk(didVerificationMethod.publicKeyJwk)
    }
}