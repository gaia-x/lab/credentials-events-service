package eu.gaiax.ces.v2.service

import eu.gaiax.ces.common.exception.*
import eu.gaiax.ces.common.service.TrustedIssuersService
import eu.gaiax.ces.v2.entity.JWTEvent
import eu.gaiax.ces.v2.utils.decodeJWT
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.transaction.Transactional
import org.jose4j.jwt.consumer.InvalidJwtException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@ApplicationScoped
class JWTCredentialEventService(
    @Inject var jwtSignatureService: JWTSignatureService, @Inject var trustedIssuersService: TrustedIssuersService
) {

    val logger: Logger = LoggerFactory.getLogger(JWTCredentialEventService::class.java)

    @Throws(
        InvalidContentException::class, InvalidSignatureException::class, ExpiredCredentialException::class
    )
    fun handleCredentialsEvent(event: JWTCloudEventDTO): UUID {
        try {
            if (event.dataBase64.isEmpty()) {
                throw InvalidContentException("Event content invalid")
            }

            if (!jwtSignatureService.isSignatureValid(event.dataBase64)) {
                throw InvalidSignatureException("Unable to validate signature of the payload")
            }

            val decodedJWT: Map<String, Any> = decodeJWT(event.dataBase64).jwtClaims.claimsMap

            if (!isValidType(decodedJWT, event.type)) {
                throw InvalidTypeException("Invalid credential type")
            }

            if (isExpired(decodedJWT)) {
                throw ExpiredCredentialException("the credential is expired")
            }

            val submittableEvent = JWTCloudEventDTO.toEvent(event)
            this.saveInDB(submittableEvent)
            return submittableEvent.id
        } catch (e: InvalidJwtException) {
            logger.error("Error decoding JWT " + e.message)
            throw InvalidContentException("Event content invalid")
        }
    }

    /**
     * Check whenever credential event type is valid or not
     * @return false if the event type is eu.gaia-x.credential and at least one credential subject
     * is not of gx:compliance type
     */
    private fun isValidType(data: Map<String, Any>, type: String): Boolean {
        if (type != "eu.gaia-x.credential") {
            if (!isValidNonGaiaXCompliance(data)) {
                throw InvalidTypeException("Non-Gaia-X credential can only contain one subject with one id")
            }
            return true
        }
        return isValidGaiaXCompliance(data)
    }

    private fun isValidNonGaiaXCompliance(data: Map<String, Any>): Boolean {
        var subjects = data["credentialSubject"] ?: return true
        if (subjects !is List<*>) {
            subjects = listOf(subjects)
        }
        return (subjects as List<*>).stream().allMatch {
            it is Map<*, *> && (it.size == 1 && (it["id"].toString().isNotEmpty() || it["@id"].toString().isNotEmpty()))
        }
    }

    private fun isValidGaiaXCompliance(data: Map<String, Any>): Boolean {
        if (!trustedIssuersService.isIssuerAllowed(data["issuer"] as String)) {
            throw InvalidIssuerException("Invalid credential issuer")
        }
        val typeList = data["type"] as List<*>
        if (!typeList.contains("gx:LabelCredential") && !typeList.contains("LabelCredential")) {
            throw InvalidTypeException("Invalid credential type")
        }
        val subject: Map<String, Any> = data["credentialSubject"] as Map<String, Any>
        val compliantCredentials = subject["gx:compliantCredentials"]
        return !(compliantCredentials == null || (compliantCredentials as List<*>).isEmpty())
    }

    private fun isExpired(data: Map<String, Any>): Boolean {
        val validUntil = ZonedDateTime.parse(data["validUntil"].toString(), DateTimeFormatter.ISO_DATE_TIME);
        return validUntil.isBefore(ZonedDateTime.now())
    }

    @Transactional
    fun saveInDB(event: JWTEvent) {
        logger.info("[eventId:{}] Saving event in DB", event.id)
        JWTEvent.persist(event)
        logger.info("[eventId:{}] Saved event in DB", event.id)
    }
}