package eu.gaiax.ces.v2.entity

import eu.gaiax.ces.v2.service.JWTCloudEventDTO
import io.hypersistence.utils.hibernate.type.json.JsonType
import io.quarkus.hibernate.orm.panache.kotlin.PanacheCompanionBase
import io.quarkus.hibernate.orm.panache.kotlin.PanacheEntityBase
import io.quarkus.panache.common.Page
import io.quarkus.panache.common.Sort
import jakarta.persistence.*
import org.hibernate.annotations.Type
import java.time.ZonedDateTime
import java.util.*

@Entity
@Table(name = "jwtEvent")
class JWTEvent : PanacheEntityBase {

    @Id
    lateinit var id: UUID

    @Column(name = "content", columnDefinition = "JSON")
    @Type(value = JsonType::class)
    lateinit var content: JWTCloudEventDTO

    lateinit var creationDate: ZonedDateTime

    companion object : PanacheCompanionBase<JWTEvent, UUID> {

        fun getPage(page: Int, size: Int, afterId: UUID?, type: String?): List<JWTEvent> {
            if (afterId == null && type.isNullOrBlank()) {
                return JWTEvent
                    .findAll(Sort.ascending("creationDate", "id"))
                    .page(Page.of(page, size))
                    .list()
            }
            val query: Query
            if (afterId == null) {
                query = getEntityManager()
                    .createNativeQuery("""
                    SELECT * FROM jwtEvent e
                    WHERE e.content ->> 'type' = :type
                    ORDER BY e.creationDate ASC, e.id ASC
                    """.trimIndent(), JWTEvent::class.java)
                    .setParameter("type", type)
            } else if (type.isNullOrBlank()) {
                query = getEntityManager()
                    .createNativeQuery("""
                    SELECT e.* FROM jwtEvent e
                    WHERE (e.creationDate, e.id) > (SELECT creationDate, id FROM jwtEvent WHERE id = :id)
                    ORDER BY e.creationDate ASC, e.id ASC
                    """.trimIndent(), JWTEvent::class.java)
                    .setParameter("id", afterId)
            } else {
                query = getEntityManager()
                    .createNativeQuery("""
                    SELECT e.* FROM jwtEvent e
                    WHERE e.content ->> 'type' = :type
                    AND (e.creationDate, e.id) > (SELECT creationDate, id FROM jwtEvent WHERE id = :id)
                    ORDER BY e.creationDate ASC, e.id ASC
                    """.trimIndent(), JWTEvent::class.java)
                    .setParameter("id", afterId)
                    .setParameter("type", type)
            }
            query.setMaxResults(size).setFirstResult(size * page)
            return query.resultList.map {
                it as JWTEvent
            }
        }

    }

}