package eu.gaiax.ces.v2.web

import eu.gaiax.ces.v2.entity.JWTEvent
import eu.gaiax.ces.v2.service.JWTCloudEventDTO
import eu.gaiax.ces.v2.service.JWTCredentialEventService
import eu.gaiax.ces.v2.service.JWTDecodedCloudEventDTO
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.resteasy.reactive.ResponseStatus
import java.net.URI
import java.util.*

@Path("v2/credentials-events")
class CredentialEventResourceJWT(val credentialEventService: JWTCredentialEventService) {

    @Operation(
        summary = "Retrieve credentials events",
        description = """This endpoint provides you a paginated list of credentials events.<br>
            You can modify the pagination starting point by providing the last ID you received, and filter by credential
            type."""
    )
    @Parameters(
        Parameter(name = "page", required = false, description = "Pagination starts at page 0", example = "0"),
        Parameter(name = "size", required = false, description = "Page size, default value 20", example = "20"),
        Parameter(
            name = "lastReceivedID", required = false,
            description = "Allows to start the pagination from just after this ID"
        ),
        Parameter(
            name = "type", required = false, description = "Type of Cloud Events to retrieve",
            example = "eu.gaia-x.credential"
        )
    )
    @Produces("application/cloudevents-batch+json; charset=UTF-8")
    @GET
    fun getCredentialsEvents(
        @QueryParam("page") @DefaultValue("0") page: Int,
        @QueryParam("size") @DefaultValue("20") size: Int,
        @QueryParam("lastReceivedID") lastReceivedID: UUID?,
        @QueryParam("type") type: String?
    ): List<JWTDecodedCloudEventDTO> {
        return JWTEvent.getPage(page, size, lastReceivedID, type).map {
            JWTCloudEventDTO.toDecodedEvent(it.id, it.creationDate, it.content)
        }
    }

    @Operation(summary = "Retrieve a single credentials event by its UUID")
    @APIResponses(
        APIResponse(responseCode = "200", description = "Event retrieved successfully"),
        APIResponse(responseCode = "404", description = "Event not found"),
        APIResponse(responseCode = "500", description = "Technical error"),
    )
    @Produces("application/cloudevents+json; charset=UTF-8")
    @GET
    @Path("/{id}")
    fun getCredentialsEventById(@PathParam("id") id: UUID): JWTDecodedCloudEventDTO {
        val event = JWTEvent.findById(id) ?: throw NotFoundException("Event $id not found")
        return JWTCloudEventDTO.toDecodedEvent(event.id, event.creationDate, event.content)
    }

    @Operation(
        summary = "Submit credentials event",
        description = "This endpoint allows you to submit cloud events containing VerifiableCredentials in VC-JWT format. The cloud event type should be eu.gaia-x.credential for gx:ComplianceCredential VCs, any other string for non-compliance credentials"
    )
    @APIResponses(
        APIResponse(responseCode = "201", description = "Event received and ready to be shared"),
        APIResponse(responseCode = "400", description = "Event format incorrect"),
        APIResponse(responseCode = "409", description = "Event data signature invalid or invalid issuer."),
        APIResponse(responseCode = "500", description = "Technical error"),
    )
    @Consumes("application/cloudevents+json", MediaType.APPLICATION_JSON)
    @POST
    @ResponseStatus(201)
    fun submitCredentialsEvent(event: JWTCloudEventDTO): Response {
        val createdEventUUID = credentialEventService.handleCredentialsEvent(event)
        return Response.created(URI.create("/v2/credentials-events/$createdEventUUID")).build()
    }
}