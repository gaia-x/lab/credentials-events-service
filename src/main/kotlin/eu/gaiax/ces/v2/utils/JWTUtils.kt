package eu.gaiax.ces.v2.utils;

import eu.gaiax.ces.common.exception.InvalidSignatureException
import org.jose4j.jwt.consumer.JwtConsumer
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.jwt.consumer.JwtContext


fun decodeJWT(token: String): JwtContext {
    val jwtConsumer: JwtConsumer =
        JwtConsumerBuilder()
            .setSkipSignatureVerification()
            .setDisableRequireSignature()
            .build()
    val joseJwt: JwtContext = jwtConsumer.process(token)

    return joseJwt
}

fun getJWTHeader(joseJwt: JwtContext, headerName: String): String {
    if (joseJwt.joseObjects.isEmpty()) {
        throw InvalidSignatureException("JWT is malformed")
    }
    return joseJwt.joseObjects[0].headers.getStringHeaderValue(headerName)
}