package eu.gaiax.ces.v1.web

import eu.gaiax.ces.v1.service.CreateVerifiableCredentialCloudEventDTO
import eu.gaiax.ces.v1.service.VerifiableCredentialCloudEvent
import eu.gaiax.ces.v1.service.VerifiableCredentialCloudEventDTO
import eu.gaiax.ces.v1.service.CredentialEventService
import eu.gaiax.ces.v1.entity.Event
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.resteasy.reactive.ResponseStatus
import java.net.URI
import java.util.*

@Deprecated("Deprecated in favour of CredentialEventResourceJWT")
@Path("/credentials-events")
class CredentialEventResource(val credentialEventService: CredentialEventService) {

    @Operation(
        deprecated = true,
        summary = "Retrieve credentials event",
        description = """This endpoint provides you a paginated list of credentials events.<br>
            You can modify the pagination starting point by providing the last ID you received, and filter by credential
            type.
            **Deprecated:** In favour of JWT, use for Tagus Credentials only.
        """
    )
    @Parameters(
        Parameter(name = "page", required = false, description = "Pagination starts at page 0", example = "0"),
        Parameter(name = "size", required = false, description = "Page size, default value 20", example = "20"),
        Parameter(
            name = "lastReceivedID",
            required = false,
            description = "Allows to start the pagination from just after this ID"
        ),
        Parameter(
            name = "type",
            required = false,
            description = "Type of Cloud Events to retrieve", example = "eu.gaia-x.credential"
        )
    )
    @Produces("application/cloudevents-batch+json; charset=UTF-8")
    @GET
    fun getCredentialsEvents(
        @QueryParam("page") @DefaultValue("0") page: Int,
        @QueryParam("size") @DefaultValue("20") size: Int,
        @QueryParam("lastReceivedID") lastReceivedID: UUID?,
        @QueryParam("type") type: String?
    ): List<VerifiableCredentialCloudEventDTO> {
        return Event.getPage(page, size, lastReceivedID, type).map {
            VerifiableCredentialCloudEvent.toDTO(it.id, it.creationDate, it.content)
        }
    }

    @Operation(
        deprecated = true,
        summary = "Retrieve a single credentials event by its UUID",
        description = """Retrieve only one single credentials event by UUID.
            **Deprecated:** In favour of JWT, use for Tagus Credentials only.
        """
    )
    @APIResponses(
        APIResponse(responseCode = "200", description = "Event retrieved successfully"),
        APIResponse(responseCode = "404", description = "Event not found"),
        APIResponse(responseCode = "500", description = "Technical error"),
    )
    @Produces("application/cloudevents+json; charset=UTF-8")
    @GET
    @Path("/{id}")
    fun getCredentialsEventById(@PathParam("id") id: UUID): VerifiableCredentialCloudEventDTO {
        val event = Event.findById(id) ?: throw NotFoundException("Event not found")
        return VerifiableCredentialCloudEvent.toDTO(event.id, event.creationDate, event.content)
    }


    @Operation(
        deprecated = true,
        summary = "Submit credentials event",
        description = """This endpoint allows you to submit VerifiableCredentials.
            The cloud event type should be eu.gaia-x.credential for gx:compliance VCs, any other string for non-compliance credentials.
            **Deprecated:** In favour of JWT, use for Tagus Credentials only."""
    )
    @APIResponses(
        APIResponse(responseCode = "201", description = "Event received and ready to be shared"),
        APIResponse(responseCode = "400", description = "Event format incorrect"),
        APIResponse(responseCode = "409", description = "Event data signature invalid or invalid issuer."),
        APIResponse(responseCode = "500", description = "Technical error"),
    )
    @Consumes("application/cloudevents+json", MediaType.APPLICATION_JSON)
    @POST
    @ResponseStatus(201)
    fun submitCredentialsEvent(event: CreateVerifiableCredentialCloudEventDTO): Response {
        val createdEventUUID =
            credentialEventService.handleCredentialsEvent(
                CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                    event
                )
            )
        return Response.created(URI.create("/credentials-events/$createdEventUUID")).build()
    }
}