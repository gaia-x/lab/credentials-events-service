package eu.gaiax.ces.v1.service

import eu.gaiax.ces.common.exception.*
import eu.gaiax.ces.common.service.TrustedIssuersService
import eu.gaiax.ces.v1.entity.Event
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@ApplicationScoped
class CredentialEventService(
    @Inject var signatureService: SignatureService, @Inject var trustedIssuersService: TrustedIssuersService
) {

    val logger: Logger = LoggerFactory.getLogger(CredentialEventService::class.java)

    @Throws(
        InvalidContentException::class, InvalidIssuerException::class, InvalidSignatureException::class
    )
    fun handleCredentialsEvent(event: VerifiableCredentialCloudEvent): UUID {
        if (event.data.isEmpty()) {
            throw InvalidContentException("Event content invalid")
        }
        if (!hasAnIssuer(event)) {
            throw InvalidIssuerException("Invalid credential issuer")
        }
        if (isInvalidSignature(event)) {
            throw InvalidSignatureException("Unable to validate signature of the payload")
        }
        if (!isValidType(event)) {
            throw InvalidTypeException("Invalid credential type")
        }

        if (isExpired(event)) {
            throw ExpiredCredentialException("the credential is expired")
        }

        val submittableEvent = VerifiableCredentialCloudEvent.toEvent(event)
        this.saveInDB(submittableEvent)
        return submittableEvent.id
    }

    /**
     * Whether the issuer of the credential is trusted
     * @return true if the issuer is valid, false otherwise
     */
    private fun hasAnIssuer(event: VerifiableCredentialCloudEvent): Boolean {
        val data = event.data
        if (!data.containsKey("issuer")) {
            return false
        }
        val issuer = data["issuer"] ?: return false
        return (issuer as String).isNotEmpty()
    }

    /**
     * Whether the signature is marked as invalid
     * @return true if the signature is invalid, false otherwise
     */
    private fun isInvalidSignature(event: VerifiableCredentialCloudEvent): Boolean {
        return !signatureService.isSignatureValid(event)
    }

    /**
     * Check whenever credential event type is valid or not
     * @return false if the event type is eu.gaia-x.credential and at least one credential subject
     * is not of gx:compliance type
     */
    private fun isValidType(event: VerifiableCredentialCloudEvent): Boolean {
        if (event.type != "eu.gaia-x.credential") {
            if (!isValidNonGaiaXCompliance(event)) {
                throw InvalidTypeException("Non-Gaia-X credential can only contain one subject with one id")
            }
            return true
        }
        return isValidGaiaXCompliance(event)
    }

    private fun isValidNonGaiaXCompliance(event: VerifiableCredentialCloudEvent): Boolean {
        var subjects = event.data["credentialSubject"] ?: return true
        if (subjects !is List<*>) {
            subjects = listOf(subjects)
        }
        return (subjects as List<*>).stream().allMatch {
            it is Map<*, *> && (it.size == 1 && (it["id"] != "" || it["@id"] != ""))
        }
    }

    private fun isValidGaiaXCompliance(event: VerifiableCredentialCloudEvent): Boolean {
        if (!trustedIssuersService.isIssuerAllowed(event.data["issuer"] as String)) {
            throw InvalidIssuerException("Invalid credential issuer")
        }
        var subjects = event.data["credentialSubject"] ?: return true
        if (subjects !is List<*>) {
            subjects = listOf(subjects)
        }
        return (subjects as List<*>).stream().allMatch {
            it is Map<*, *> && (it["type"] == "gx:compliance" || it["@type"] == "gx:compliance")
        }
    }

    private fun isExpired(event: VerifiableCredentialCloudEvent): Boolean {
        val validUntil = ZonedDateTime.parse(event.data["expirationDate"].toString(), DateTimeFormatter.ISO_DATE_TIME);
        return validUntil.isBefore(ZonedDateTime.now())
    }

    @Transactional
    fun saveInDB(event: Event) {
        logger.info("[eventId:{}] Saving event in DB", event.id)
        Event.persist(event)
        logger.info("[eventId:{}] Saved event in DB", event.id)
    }
}