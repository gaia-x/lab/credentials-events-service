package eu.gaiax.ces.v1.service;

import com.apicatalog.jsonld.document.JsonDocument
import foundation.identity.jsonld.ConfigurableDocumentLoader
import jakarta.annotation.PostConstruct
import jakarta.enterprise.context.ApplicationScoped
import java.net.URI

@ApplicationScoped
class StaticDocumentLoaderService: ConfigurableDocumentLoader() {

    @PostConstruct
    fun init() {
        cacheLocalDocument("credentials_v1_context.json", "https://www.w3.org/2018/credentials/v1")
        cacheLocalDocument("jws2020_v1_context.json", "https://w3id.org/security/suites/jws-2020/v1")
        cacheLocalDocument("trustframework_context.json", "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#")
        cacheLocalDocument("credentials_v2_context.json", "https://www.w3.org/ns/credentials/v2")
        cacheLocalDocument("gaiax_2404.json", "https://w3id.org/gaia-x/development#")
        isEnableLocalCache = true;
        isEnableHttp = true
        isEnableHttps = true
    }

    private fun cacheLocalDocument(name: String, context: String) {
        val resourceStream = this::class.java.classLoader.getResourceAsStream(name)
        resourceStream?.use {
            localCache[URI(context)] = JsonDocument.of(resourceStream)
        }
    }

}
