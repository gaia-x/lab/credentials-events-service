package eu.gaiax.ces.v1.service

import com.apicatalog.jsonld.JsonLd
import com.apicatalog.jsonld.JsonLdOptions
import com.apicatalog.jsonld.document.Document
import com.apicatalog.jsonld.document.JsonDocument
import com.apicatalog.rdf.io.nquad.NQuadsWriter
import foundation.identity.did.DIDDocument
import io.setl.rdf.normalization.RdfNormalize
import jakarta.enterprise.context.ApplicationScoped
import jakarta.json.Json
import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jwk.JsonWebKey
import org.jose4j.jws.AlgorithmIdentifiers
import org.jose4j.jws.JsonWebSignature
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uniresolver.UniResolver
import java.io.ByteArrayInputStream
import java.io.StringWriter
import java.net.URI
import java.security.MessageDigest


private const val PROOF = "proof"
private const val JWS = "jws"
private const val VERIFICATION_METHOD = "verificationMethod"

/**
 * Returns ASCII formatted ByteArray
 */
fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }

@ApplicationScoped
class SignatureService(
    val uniResolver: UniResolver,
    val staticDocumentLoader: StaticDocumentLoaderService
) {
    val logger: Logger = LoggerFactory.getLogger(SignatureService::class.java)

    /**
     * Whether the event signature is valid
     * @return true if the signature is valid, false otherwise
     */
    fun isSignatureValid(event: VerifiableCredentialCloudEvent): Boolean {
        val data = event.data
        val eventId = event.data.getOrElse("id") { "unknown-id" }
        logger.info("[event:{}]Validating signature", eventId)
        val proof = if (data[PROOF] != null) data[PROOF] as Map<*, *> else return false
        val signature = if (proof[JWS] != null) proof[JWS] as String else return false
        val verificationMethod =
            if (proof[VERIFICATION_METHOD] != null) proof[VERIFICATION_METHOD] as String else return false

        val did = this.resolveDID(data["issuer"] as String) ?: return false
        val key = this.retrievePublicKey(did, verificationMethod) ?: return false

        val rdfStringWithoutProof = credentialToRDFStringWithoutProof(data)
        val hashedPayload = this.sha256(rdfStringWithoutProof)

        val isSignatureValid = verifySignature(hashedPayload, signature, key)
        logger.info(
            "[event:{} - isValid:{}]Validated signature",
            eventId,
            isSignatureValid
        )
        return isSignatureValid
    }

    private fun sha256(rdfStringWithoutProof: String): String {
        val digest = MessageDigest.getInstance("SHA-256")
        return digest.digest(rdfStringWithoutProof.toByteArray()).toHex()
    }

    /**
     * Canonize a JSONObject into RDF via URDNA2015 canonization
     * @return a String containing the RDF representation of the JSON
     */
    private fun credentialToRDFStringWithoutProof(credential: Map<String, Any>): String {
        val clone = Json.createObjectBuilder(credential).remove(PROOF).build()

        val options = JsonLdOptions(staticDocumentLoader)
        val document: Document = JsonDocument.of(ByteArrayInputStream(clone.toString().toByteArray()))
        val rdfDataset = RdfNormalize.normalize(JsonLd.toRdf(document).options(options).get())
        val stringWriter = StringWriter()
        val writer = NQuadsWriter(stringWriter)
        writer.write(rdfDataset)
        return stringWriter.toString()
    }

    private fun verifySignature(hashedPayload: String, signature: String, publicKey: JsonWebKey): Boolean {
        val jws = JsonWebSignature()
        jws.compactSerialization = signature
        jws.setAlgorithmConstraints(
            AlgorithmConstraints(
                AlgorithmConstraints.ConstraintType.PERMIT,
                AlgorithmIdentifiers.RSA_USING_SHA256,
                AlgorithmIdentifiers.ECDSA_USING_P256_CURVE_AND_SHA256,
                AlgorithmIdentifiers.RSA_PSS_USING_SHA256
            )
        )
        jws.payload = hashedPayload
        jws.key = publicKey.key
        return jws.verifySignature()
    }

    private fun resolveDID(did: String): DIDDocument? {
        return try {
            uniResolver.resolve(did).didDocument
        } catch (exception: Exception) {
            this.logger.warn("Unable to resolve did {} - {}", did, exception.message)
            null
        }
    }

    private fun retrievePublicKey(did: DIDDocument, verificationMethod: String): JsonWebKey? {
        val didVerificationMethod = did.allVerificationMethodsAsMap[URI.create(verificationMethod)] ?: return null
        return JsonWebKey.Factory.newJwk(didVerificationMethod.publicKeyJwk)
    }
}