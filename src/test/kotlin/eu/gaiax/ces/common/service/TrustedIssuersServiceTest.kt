package eu.gaiax.ces.common.service

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import eu.gaiax.ces.common.exception.InvalidIssuerException
import io.quarkus.test.junit.QuarkusTest
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test


@QuarkusTest
class TrustedIssuersServiceTest {
    companion object {
        var wireMockServer = WireMockServer(options().dynamicPort())

        @BeforeAll
        @JvmStatic
        fun setUp() {
            wireMockServer.start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            wireMockServer.stop()
        }
    }

    private final val issuer = "did:web:compliance.lab.gaia-x.eu:v1"
    private final val complexIssuer = "did:web:registration.lab.gaia-x.eu:development"
    private final val multipleIssuers = """
        [
          "compliance.lab.gaia-x.eu/v1",
          "gx-compliance.aruba.it/v1"
        ]
    """.trimIndent()

    @Test
    fun shouldReturnFalse_whenIssuerIsNotInListRetrievedFromSource() {
        wireMockServer.stubFor(get("/").willReturn(ok(multipleIssuers)))
        val trustedIssuersService = TrustedIssuersService(wireMockServer.baseUrl(), false)
        assertThat(trustedIssuersService.isIssuerAllowed(issuer)).isTrue()
    }

    @Test
    fun shouldThrowInvalidIssuerException_whenIssuerListIsNotResolved() {
        wireMockServer.stubFor(get("/").willReturn(badRequest()))
        val trustedIssuersService = TrustedIssuersService(wireMockServer.baseUrl(), false)
        Assertions.assertThatThrownBy { trustedIssuersService.isIssuerAllowed(issuer) }
            .isInstanceOf(InvalidIssuerException::class.java)
    }

    @Test
    fun shouldThrowInvalidIssuerException_whenIssuerListResolutionFails() {
        wireMockServer.stubFor(get("/").willReturn(ok()))
        val trustedIssuersService = TrustedIssuersService(wireMockServer.baseUrl(), false)
        Assertions.assertThatThrownBy { trustedIssuersService.isIssuerAllowed(issuer) }
            .isInstanceOf(InvalidIssuerException::class.java)
    }

    @Test
    fun shouldReturnTrue_whenIssuerIsInListRetrievedFromSource() {
        wireMockServer.stubFor(get("/").willReturn(ok(multipleIssuers)))
        val trustedIssuersService = TrustedIssuersService(wireMockServer.baseUrl(), false)
        assertThat(trustedIssuersService.isIssuerAllowed(issuer)).isTrue()
    }

    @Test
    fun shouldReturnTrue_whenIssuerCheckIsDisabled() {
        val trustedIssuersService = TrustedIssuersService(wireMockServer.baseUrl(), true)
        assertThat(trustedIssuersService.isIssuerAllowed(issuer)).isTrue()

    }

    @Test
    fun shouldReturnDidBaseURL() {
        assertThat(TrustedIssuersService.prepareDIDForResolution(issuer)).isEqualTo("compliance.lab.gaia-x.eu/v1")
    }

    @Test
    fun shouldReturnDidBaseURL_onComplexIssuer() {
        assertThat(TrustedIssuersService.prepareDIDForResolution(complexIssuer)).isEqualTo("registration.lab.gaia-x.eu/development")
    }
}