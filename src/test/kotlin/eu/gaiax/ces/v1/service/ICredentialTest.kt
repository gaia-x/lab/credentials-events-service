package eu.gaiax.ces.v1.service

import com.fasterxml.jackson.databind.ObjectMapper
import foundation.identity.did.DIDDocument

class ICredentialTest(objectMapper: ObjectMapper) {
    var unproperlySignedVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var invalidIssuerSignedVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var signedVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var emptyVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var missingSignatureVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var invalidVerifiableCredentialCloudEventOtherType: VerifiableCredentialCloudEvent
    var nonGaiaXEventContainingMoreInfoThanId: VerifiableCredentialCloudEvent
    var nonGaiaXEventContainingOnlyID: VerifiableCredentialCloudEvent
    var expiredVerifiableCredentialCloudEvent: VerifiableCredentialCloudEvent
    var complianceDID: DIDDocument
    var complianceV1DID: DIDDocument
    var localhostDID: DIDDocument

    init {
        val resource = this::class.java.getResourceAsStream("/v1/unproperly_signed_compliance_cloud_event.json")
        resource.use {
            unproperlySignedVerifiableCredentialCloudEvent = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    resource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val signedComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v1/signed_compliance_cloud_event.json")
        signedComplianceCloudEventResource.use {
            signedVerifiableCredentialCloudEvent = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    signedComplianceCloudEventResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val emptyComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v1/empty_compliance_cloud_event.json")
        emptyComplianceCloudEventResource.use {
            emptyVerifiableCredentialCloudEvent = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    emptyComplianceCloudEventResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val missingSignatureComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v1/missing_signature_compliance_cloud_event.json")
        missingSignatureComplianceCloudEventResource.use {
            missingSignatureVerifiableCredentialCloudEvent = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    missingSignatureComplianceCloudEventResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val invalidIssuerSignedComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v1/invalid_issuer_signed_compliance_cloud_event.json")
        invalidIssuerSignedComplianceCloudEventResource.use {
            invalidIssuerSignedVerifiableCredentialCloudEvent =
                CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                    objectMapper.readValue(
                        invalidIssuerSignedComplianceCloudEventResource,
                        CreateVerifiableCredentialCloudEventDTO::class.java
                    )
                )
        }
        val complianceDIDResource = this::class.java.getResourceAsStream("/v1/did/compliance_did.json")
        complianceDIDResource.use {
            complianceDID = DIDDocument.fromJson(
                String(complianceDIDResource!!.readAllBytes())
            )
        }
        val complianceV1DIDResource = this::class.java.getResourceAsStream("/v1/did/compliance_v1_did.json")
        complianceV1DIDResource.use {
            complianceV1DID = DIDDocument.fromJson(
                String(complianceV1DIDResource!!.readAllBytes())
            )
        }
        val localhostDIDResource = this::class.java.getResourceAsStream("/v1/did/localhost_did.json")
        localhostDIDResource.use {
            localhostDID = DIDDocument.fromJson(
                String(localhostDIDResource!!.readAllBytes())
            )
        }
        val invalidSignedComplianceCloudEventOtherTypeResource =
            this::class.java.getResourceAsStream("/v1/invalid_compliance_cloud_event_other_type.json")
        invalidSignedComplianceCloudEventOtherTypeResource.use {
            invalidVerifiableCredentialCloudEventOtherType = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    invalidSignedComplianceCloudEventOtherTypeResource,
                    CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val nonGaiaXEventContainingMoreThanInfoResource =
            this::class.java.getResourceAsStream("/v1/non_gaiax_event_containing_more_than_id.json")
        nonGaiaXEventContainingMoreThanInfoResource.use {
            nonGaiaXEventContainingMoreInfoThanId = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    nonGaiaXEventContainingMoreThanInfoResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val nonGaiaXEventContainingOnlyIDResource =
            this::class.java.getResourceAsStream("/v1/non_gaiax_event_containing_only_id.json")
        nonGaiaXEventContainingOnlyIDResource.use {
            nonGaiaXEventContainingOnlyID = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    nonGaiaXEventContainingOnlyIDResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
        val expiredVerifiableCredentialCloudEventResource =
            this::class.java.getResourceAsStream("/v1/expired_compliance_cloud_event.json")
        expiredVerifiableCredentialCloudEventResource.use {
            expiredVerifiableCredentialCloudEvent = CreateVerifiableCredentialCloudEventDTO.toComplianceEvent(
                objectMapper.readValue(
                    expiredVerifiableCredentialCloudEventResource, CreateVerifiableCredentialCloudEventDTO::class.java
                )
            )
        }
    }

}