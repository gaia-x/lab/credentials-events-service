package eu.gaiax.ces.v1.service

import com.fasterxml.jackson.databind.ObjectMapper
import eu.gaiax.ces.common.exception.*
import eu.gaiax.ces.common.service.TrustedIssuersService
import io.quarkus.test.InjectMock
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.annotation.PostConstruct
import jakarta.inject.Inject
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when`
import org.mockito.kotlin.anyVararg

@QuarkusTest
@TestTransaction
class CredentialEventServiceTest {
    lateinit var iCredentialTest: ICredentialTest

    @Inject
    lateinit var credentialEventService: CredentialEventService

    @InjectMock
    lateinit var signatureService: SignatureService

    @InjectMock
    lateinit var trustedIssuersService: TrustedIssuersService

    @Inject
    lateinit var objMapper: ObjectMapper


    @PostConstruct
    fun setUp() {
        iCredentialTest = ICredentialTest(this.objMapper)
    }


    @Test
    fun shouldThrowAInvalidSignatureException_whenSignatureIsInvalid() {
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(false)
        Assertions.assertThatThrownBy {
            credentialEventService.handleCredentialsEvent(
                iCredentialTest.unproperlySignedVerifiableCredentialCloudEvent
            )
        }.isInstanceOf(InvalidSignatureException::class.java)
    }

    @Test
    fun shouldThrowAnInvalidContentException_whenDataIsNotAComplianceCredential() {
        Assertions.assertThatThrownBy {
            credentialEventService.handleCredentialsEvent(
                iCredentialTest.emptyVerifiableCredentialCloudEvent
            )
        }.isInstanceOf(InvalidContentException::class.java)
    }

    @Test
    fun shouldThrowAInvalidIssuerException_whenDataIssuerIsNotInTrustedList() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(false)
        Assertions.assertThatThrownBy {
            credentialEventService.handleCredentialsEvent(
                iCredentialTest.invalidIssuerSignedVerifiableCredentialCloudEvent
            )
        }.isInstanceOf(InvalidIssuerException::class.java)
    }


    @Test
    fun shouldDoNothing_whenSignatureIsValid() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        assertDoesNotThrow(fun() {
            credentialEventService.handleCredentialsEvent(iCredentialTest.signedVerifiableCredentialCloudEvent)
        })
    }

    @Test
    @DisplayName("should throw an error when event type is eu.gaia-x.credential and no subject of type gx:compliance")
    fun shouldThrowTypeException_whenTypeIsGxCredentialAndAnySubjectIsNotCompliance() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        assertThrows<InvalidTypeException> {
            credentialEventService.handleCredentialsEvent(iCredentialTest.invalidVerifiableCredentialCloudEventOtherType)
        }
    }

    @Test
    @DisplayName("should throw an error when event type is not eu.gaia-x.credential and VC contains an ID and an another field")
    fun shouldThrowTypeException_whenTypeIsNotGxComplianceAndContainsSomethingElseThanID() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        assertThrows<InvalidTypeException> {
            credentialEventService.handleCredentialsEvent(iCredentialTest.nonGaiaXEventContainingMoreInfoThanId)
        }
    }

    @Test
    @DisplayName("should accept when event type is not eu.gaia-x.credential and VC contains only an ID")
    fun shouldDoNothing_whenTypeIsNotGxComplianceAndContainsOnlyID() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        assertDoesNotThrow {
            credentialEventService.handleCredentialsEvent(iCredentialTest.nonGaiaXEventContainingOnlyID)
        }
    }

    @Test
    @DisplayName("should throw an exception when credentials are expired")
    fun shouldThrowExpiredException_whenCredentialIsExpired() {
        `when`(signatureService.isSignatureValid(anyVararg(VerifiableCredentialCloudEvent::class))).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        assertThrows<ExpiredCredentialException> {
            credentialEventService.handleCredentialsEvent(iCredentialTest.expiredVerifiableCredentialCloudEvent)
        }
    }
}