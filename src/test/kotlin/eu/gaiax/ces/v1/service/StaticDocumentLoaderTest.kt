package eu.gaiax.ces.v1.service

import com.apicatalog.jsonld.loader.DocumentLoaderOptions
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.net.URI

@QuarkusTest
class StaticDocumentLoaderTest {

    @Inject
    lateinit var documentLoader: StaticDocumentLoaderService

    @Test
    fun shouldReturnDocument_whenIsStaticResource() {
        val document =
            documentLoader.loadDocument(URI("https://www.w3.org/2018/credentials/v1"), DocumentLoaderOptions())
        Assertions.assertThat(document.jsonContent).isPresent()
        val vc = document.jsonContent.map { it.asJsonObject()["@context"]?.asJsonObject()?.get("VerifiableCredential") }
            .orElse(null)
        Assertions.assertThat(vc).isNotNull()
    }

    @Test
    fun shouldReturnDocument_whenIsWebResource() {
        val document =
            documentLoader.loadDocument(URI("https://json-ld.org/contexts/person.jsonld"), DocumentLoaderOptions())
        Assertions.assertThat(document.jsonContent).isPresent()
    }

    @Test
    fun shouldThrowError_whenResourceNotFound() {
        assertThrows<Exception> {
            documentLoader.loadDocument(URI("https://not-a-scheme.foo.bar"), DocumentLoaderOptions())
        }
    }

}