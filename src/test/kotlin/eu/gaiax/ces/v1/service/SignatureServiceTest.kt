package eu.gaiax.ces.v1.service

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.annotation.PostConstruct
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import uniresolver.ResolutionException
import uniresolver.UniResolver
import uniresolver.result.ResolveDataModelResult

@QuarkusTest
class SignatureServiceTest {
    @Inject
    lateinit var objMapper: ObjectMapper
    lateinit var iCredentialTest: ICredentialTest

    @InjectMock
    lateinit var uniResolver: UniResolver

    @Inject
    lateinit var signatureService: SignatureService

    @PostConstruct
    fun setUp() {
        iCredentialTest = ICredentialTest(this.objMapper)
    }


    @Test
    fun shouldReturnTrue_whenSignatureIsValid() {
        val result = ResolveDataModelResult.build()
        result.didDocument = iCredentialTest.complianceV1DID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThat(signatureService.isSignatureValid(iCredentialTest.signedVerifiableCredentialCloudEvent)).isTrue()
    }

    @Test
    fun shouldReturnFalse_whenSignatureIsInvalid() {
        val result = ResolveDataModelResult.build()
        result.didDocument = iCredentialTest.complianceDID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThat(signatureService.isSignatureValid(iCredentialTest.unproperlySignedVerifiableCredentialCloudEvent)).isFalse()
    }

    @Test
    fun shouldReturnFalse_whenSignatureIsMissing() {
        val result = ResolveDataModelResult.build()
        result.didDocument = iCredentialTest.complianceDID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThat(signatureService.isSignatureValid(iCredentialTest.missingSignatureVerifiableCredentialCloudEvent)).isFalse()
    }

    @Test
    fun shouldReturnFalse_whenSignatureVerificationMethodNotInDID() {
        val result = ResolveDataModelResult.build()
        result.didDocument = iCredentialTest.localhostDID
        `when`(uniResolver.resolve(anyString())).thenReturn(result)
        assertThat(signatureService.isSignatureValid(iCredentialTest.signedVerifiableCredentialCloudEvent)).isFalse()
    }

    @Test
    fun shouldReturnFalse_whenDIDIsNotReachable() {
        `when`(uniResolver.resolve(anyString())).thenThrow(ResolutionException("Unable to reach DID"))
        assertThat(signatureService.isSignatureValid(iCredentialTest.signedVerifiableCredentialCloudEvent)).isFalse()
    }
}