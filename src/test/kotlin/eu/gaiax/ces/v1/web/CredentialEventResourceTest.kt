package eu.gaiax.ces.v1.web

import eu.gaiax.ces.v1.entity.Event
import eu.gaiax.ces.v1.service.VerifiableCredentialCloudEvent
import eu.gaiax.ces.v1.service.VerifiableCredentialCloudEventDTO
import foundation.identity.did.DIDDocument
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import jakarta.transaction.Transactional
import jakarta.ws.rs.core.MediaType
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.anything
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.kotlin.whenever
import uniresolver.UniResolver
import uniresolver.result.ResolveDataModelResult
import java.time.ZonedDateTime
import java.util.*

@QuarkusTest
class CredentialEventResourceTest {

    @InjectMock
    lateinit var didResolver: UniResolver

    @BeforeEach
    fun beforeEach() {
        var complianceDID: String
        val complianceDIDResource = this::class.java.getResourceAsStream("/v1/did/compliance_did.json")
        complianceDIDResource.use {
            complianceDID = String(complianceDIDResource!!.readAllBytes())
        }
        val result = ResolveDataModelResult.build(null, DIDDocument.fromJson(complianceDID), null)

        var complianceV1DID: String
        val complianceV1DIDResource = this::class.java.getResourceAsStream("/v1/did/compliance_v1_did.json")
        complianceDIDResource.use {
            complianceV1DID = String(complianceV1DIDResource!!.readAllBytes())
        }
        val complianceV1DIDResult = ResolveDataModelResult.build(null, DIDDocument.fromJson(complianceV1DID), null)

        whenever(didResolver.resolve(eq("did:web:compliance.lab.gaia-x.eu:development"))).thenReturn(
            result
        )
        whenever(didResolver.resolve(eq("did:web:wizard.lab.gaia-x.eu:api:credentials:2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8D1AtEtzMiGfugZizK8LXFKHWuLP5Ly9DcFzYYKxyk3Nzk9j3krjSUca7HD7AEf16w9Jna6xBChsYj9aDJbWmXoJZujTafD4vuqMbS6kGCQtcNzpAurLRoBiS69WbqRZxNzSokMPn4fZGQJWEGEwEp4xmw9yzHZjyhhNjengNaaMWAYKgty1X7mGF2TR5eg1sh6vYucp1NFhngaR8Rajaz8M9RVjRbvWWZxtnngNU2cbWoSLg8ZjfQBJhTvMKnmUxsVD9dQHwPSeCD38nJaxt2Gr7Yxk7b8eDF521VG9DNQ4brZzB93kpgpWRNAPbfyxBBXTjTJjZr3jHDSHJTxuoxdXduZY4do9QEvrggU2iv9Zm16sDsqqHtfXm2TMTHRSuSThEQdF7RdajDxW55Jnn4y1ogVEkJbVf8yWzD5wBksMHt4HndxvoPSegnLuRHmc2ZE1H8ChPvGQEpSrUjWw6xzNHGHYQbQd1988zJHduaYULZDKDtBX5W4z5uku7BqYxS2vfxg"))).thenReturn(
            result
        )
        whenever(didResolver.resolve(eq("did:web:compliance.lab.gaia-x.eu:v1-staging"))).thenReturn(
            complianceV1DIDResult
        )
    }

    @Test
    fun shouldReturnConflict_whenMissingTheIssuer() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/invalid_issuer_signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/credentials-events")
            .then()
            .statusCode(409).body(
                `is`(
                    "{\"message\":\"Invalid credential issuer\"}"
                )
            )
    }

    @Test
    fun shouldReturnConflict_whenSignatureIsInvalid() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/missing_signature_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given().body(content).headers("Content-Type", "application/cloudevents+json; charset=UTF-8").`when`()
            .post("/credentials-events")
            .then().statusCode(409).body(
                `is`(
                    "{\"message\":\"Unable to validate signature of the payload\"}"
                )
            )
    }

    @Test
    fun shouldReturnBadRequest_whenContentIsMissing() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/empty_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/credentials-events")
            .then().statusCode(400).body(
                `is`(
                    "{\"message\":\"Event content invalid\"}"
                )
            )
    }

    @Test
    fun shouldReturnBadRequest_whenTimeIsMissing() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/missing_time_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/credentials-events")
            .then().statusCode(400).body(
                `is`(
                    "{\"message\":\"At least one field is missing in the event [time]\"}"
                )
            )
    }

    @Test
    @Transactional
    fun shouldReturnCreatedAndAnID_whenContentIsOk() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        val response = given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/credentials-events")
            .thenReturn()

        response.then().statusCode(201).header("Location", anything())
        val createdUUID = UUID.fromString(response.header("Location").split("/").last())
        Event.deleteById(createdUUID)
    }

    @Test
    @Transactional
    fun shouldReturnCreatedAndAnID_whenContentIsOkAndContentTypeIsJson() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        val response = given()
            .body(content)
            .headers("Content-Type", MediaType.APPLICATION_JSON)
            .`when`()
            .post("/credentials-events")
        response.prettyPrint()
        response.then().statusCode(201).header("Location", anything())
        val createdUUID = UUID.fromString(response.header("Location").split("/").last())
        Event.deleteById(createdUUID)
    }

    @Test
    fun shouldReturnAllItemsInDatabase_whenPageAndSizeNotProvided() {
        val response = given().`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
            eventOtherType.id.toString(),
            eventOtherType2.id.toString(),
        )
    }

    @Test
    fun shouldReturnFirstItemInDatabase_whenPageZeroAndSizeOne() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event1.id.toString())
    }

    @Test
    fun shouldReturnSecondItemInDatabase_whenPageOneAndSizeOne() {
        val response =
            given()
                .queryParam("page", "1")
                .queryParam("size", "1")
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event2.id.toString())
    }

    @Test
    fun shouldReturnAllItemsInDatabase_whenPageOneAndSizeTwenty() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "20")
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
            eventOtherType.id.toString(),
            eventOtherType2.id.toString()
        )
    }

    @Test
    fun shouldReturnSecondItemInDatabase_whenPageZeroAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", event1.id.toString())
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event2.id.toString())
    }

    @Test
    fun shouldReturnThirdItemInDatabase_whenPageZeroAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", event2.id.toString())
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event3.id.toString())
    }

    @Test
    fun shouldReturnEmptyPage_whenPageTwoAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "2")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", eventOtherType2.id.toString())
                .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).isEmpty()
    }

    @Test
    fun shouldReturnEventById_whenEventExists() {
        val response = given()
            .`when`()
            .get("/credentials-events/${event1.id}")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(200)

        val returnedEvent = response.jsonPath().getObject("", VerifiableCredentialCloudEventDTO::class.java)
        assertThat(returnedEvent.id).isEqualTo(event1.id)

        assertThat(returnedEvent.type).isEqualTo(event1.content.type)
    }

    @Test
    fun shouldReturnEventWithCreationDate_whenEventExists() {
        val response = given()
            .`when`()
            .get("/credentials-events/${event1.id}")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(200)

        val returnedEvent = response.jsonPath().getObject("", VerifiableCredentialCloudEventDTO::class.java)
        assertThat(returnedEvent.cesReceivedDate).isNotNull()
    }

    @Test
    fun shouldReturnNotFound_whenEventDoesNotExist() {
        val nonExistentId = UUID.randomUUID()
        val response = given()
            .`when`()
            .get("/credentials-events/$nonExistentId")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(404)
    }

    @Test
    fun shouldReturnCorrectContentType_whenSingleEvent() {
        val response = given()
            .`when`()
            .get("/credentials-events/${event1.id}")
            .andReturn()

        val normalizedType = response.contentType.replace("; ", ";")
        assertThat(normalizedType).isEqualTo("application/cloudevents+json;charset=UTF-8")
    }

    @Test
    fun shouldReturnCorrectContentType_whenMultipleEvents() {
        val response = given()
            .`when`()
            .get("/credentials-events")
            .andReturn()

        val normalizedType = response.contentType.replace("; ", ";")
        assertThat(normalizedType).isEqualTo("application/cloudevents-batch+json;charset=UTF-8")
    }

    @Test
    fun shouldReturnComplianceItemsInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "eu.gaia-x.credential")
            .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
        )
    }

    @Test
    fun shouldReturnOtherTypeItemInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .queryParam("page", "0")
            .queryParam("size", "1")
            .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType.id.toString(),
        )
    }

    @Test
    fun shouldReturnOtherTypeItemsInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType.id.toString(),
            eventOtherType2.id.toString()
        )
    }


    @Test
    fun shouldReturnNextOtherTypeItemsInDatabase_whenTypeProvidedAndLastReceivedIDProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .queryParam("lastReceivedID", eventOtherType.id)
            .`when`().get("/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType2.id.toString()
        )
    }


    companion object {
        lateinit var event1: Event
        lateinit var event2: Event
        lateinit var event3: Event
        lateinit var eventOtherType: Event
        lateinit var eventOtherType2: Event

        @BeforeAll
        @JvmStatic
        @Transactional
        fun setUp() {
            event1 = Event()
            event1.id = UUID.fromString("e04ef77f-83f2-4dcc-9a14-361029534d7f")
            event1.creationDate = ZonedDateTime.now().minusSeconds(1)
            event1.content = VerifiableCredentialCloudEvent(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", HashMap()
            )

            event2 = Event()
            event2.id = UUID.fromString("bf215590-bb87-4093-88df-893b7eb6c796")
            event2.creationDate = ZonedDateTime.now()
            event2.content = VerifiableCredentialCloudEvent(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", HashMap()
            )

            // event3 is created at the same time as event2. Its uuid is greater so it is presented after event2
            event3 = Event()
            event3.id = UUID.fromString("d93e1336-1a8f-4f8b-9b9a-2cd546ad9da2")
            event3.creationDate = event2.creationDate
            event3.content = VerifiableCredentialCloudEvent(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", HashMap()
            )

            // eventOtherType is of type "edu.example.credential"
            eventOtherType = Event()
            eventOtherType.id = UUID.fromString("aaae9227-0cde-daca-9b9a-2cd546ad9ccc")
            eventOtherType.creationDate = ZonedDateTime.now().plusMinutes(5)
            eventOtherType.content = VerifiableCredentialCloudEvent(
                "1.0", "edu.example.credential", "", null, null, ZonedDateTime.now(), "application/json", HashMap()
            )

            eventOtherType2 = Event()
            eventOtherType2.id = UUID.fromString("0825ba37-e9c2-416a-b1e5-dcd037f94d5f")
            eventOtherType2.creationDate = ZonedDateTime.now().plusMinutes(5)
            eventOtherType2.content = VerifiableCredentialCloudEvent(
                "1.0", "edu.example.credential", "", null, null, ZonedDateTime.now(), "application/json", HashMap()
            )

            Event.persist(event1, event2, event3, eventOtherType, eventOtherType2)
            Event.flush()
        }

        @AfterAll
        @Transactional
        @JvmStatic
        fun tearDown() {
            Event.deleteAll()
            Event.flush()
        }
    }
}