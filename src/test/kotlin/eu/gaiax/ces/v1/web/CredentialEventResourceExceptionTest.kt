package eu.gaiax.ces.v1.web

import eu.gaiax.ces.v1.service.CredentialEventService
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.hamcrest.CoreMatchers.containsString
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import uniresolver.UniResolver

@QuarkusTest
class CredentialEventResourceExceptionTest {

    @InjectMock
    lateinit var didResolver: UniResolver

    @InjectMock
    lateinit var credentialEventService: CredentialEventService

    @Test
    fun shouldReturnInternalServerError_onUnmanagedException() {
        whenever(credentialEventService.handleCredentialsEvent(any())).thenThrow(RuntimeException::class.java)
        var content: String
        val resource = this::class.java.getResourceAsStream("/v1/signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/credentials-events")
            .then().statusCode(500).body(
                containsString(
                    "An error occurred on the server side"
                )
            )
    }
}