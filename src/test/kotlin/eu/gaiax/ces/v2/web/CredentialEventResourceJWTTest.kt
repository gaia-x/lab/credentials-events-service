package eu.gaiax.ces.v2.web

import eu.gaiax.ces.v2.entity.JWTEvent
import eu.gaiax.ces.v2.service.JWTCloudEventDTO
import eu.gaiax.ces.v2.service.JWTDecodedCloudEventDTO
import foundation.identity.did.DIDDocument
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import jakarta.transaction.Transactional
import jakarta.ws.rs.core.MediaType
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.anything
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.kotlin.whenever
import uniresolver.UniResolver
import uniresolver.result.ResolveDataModelResult
import java.time.ZonedDateTime
import java.util.*

@QuarkusTest
class CredentialEventResourceJWTTest {

    @InjectMock
    lateinit var didResolver: UniResolver

    @BeforeEach
    fun beforeEach() {
        var complianceDID: String
        val complianceDIDResource = this::class.java.getResourceAsStream("/v2/did/compliance_did.json")
        complianceDIDResource.use {
            complianceDID = String(complianceDIDResource!!.readAllBytes())
        }
        val result = ResolveDataModelResult.build(null, DIDDocument.fromJson(complianceDID), null)

        whenever(didResolver.resolve(eq("did:web:compliance.lab.gaia-x.eu:main"))).thenReturn(
            result
        )
    }

    @Test
    fun shouldReturnConflict_whenSignatureIsInvalid() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v2/missing_signature_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given().body(content).headers("Content-Type", "application/cloudevents+json; charset=UTF-8").`when`()
            .post("/v2/credentials-events")
            .then().statusCode(409).body(
                `is`(
                    "{\"message\":\"Unable to validate signature of the payload\"}"
                )
            )
    }

    @Test
    fun shouldReturnBadRequest_whenContentIsMissing() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v2/empty_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/v2/credentials-events")
            .then().statusCode(400).body(
                `is`(
                    "{\"message\":\"Event content invalid\"}"
                )
            )
    }

    @Test
    fun shouldReturnBadRequest_whenTimeIsMissing() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v2/missing_time_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/v2/credentials-events")
            .then().statusCode(400).body(
                `is`(
                    "{\"message\":\"At least one field is missing in the event [time]\"}"
                )
            )
    }

    @Test
    @Transactional
    fun shouldReturnCreatedAndAnID_whenContentIsOk() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v2/signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        val response = given()
            .body(content)
            .headers("Content-Type", "application/cloudevents+json; charset=UTF-8")
            .`when`()
            .post("/v2/credentials-events")
            .thenReturn()

        response.then().statusCode(201).header("Location", anything())
        val createdUUID = UUID.fromString(response.header("Location").split("/").last())
        JWTEvent.deleteById(createdUUID)
    }

    @Test
    @Transactional
    fun shouldReturnCreatedAndAnID_whenContentIsOkAndContentTypeIsJson() {
        var content: String
        val resource = this::class.java.getResourceAsStream("/v2/signed_compliance_cloud_event.json")
        resource.use {
            content = String(resource!!.readAllBytes())
        }
        val response = given()
            .body(content)
            .headers("Content-Type", MediaType.APPLICATION_JSON)
            .`when`()
            .post("/v2/credentials-events")
        response.prettyPrint()
        response.then().statusCode(201).header("Location", anything())
        val createdUUID = UUID.fromString(response.header("Location").split("/").last())
        JWTEvent.deleteById(createdUUID)
    }

    @Test
    fun shouldReturnAllItemsInDatabase_whenPageAndSizeNotProvided() {
        val response = given().`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
            eventOtherType.id.toString(),
            eventOtherType2.id.toString(),
        )
    }

    @Test
    fun shouldReturnFirstItemInDatabase_whenPageZeroAndSizeOne() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event1.id.toString())
    }

    @Test
    fun shouldReturnSecondItemInDatabase_whenPageOneAndSizeOne() {
        val response =
            given()
                .queryParam("page", "1")
                .queryParam("size", "1")
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event2.id.toString())
    }

    @Test
    fun shouldReturnAllItemsInDatabase_whenPageOneAndSizeTwenty() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "20")
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
            eventOtherType.id.toString(),
            eventOtherType2.id.toString()
        )
    }

    @Test
    fun shouldReturnSecondItemInDatabase_whenPageZeroAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", event1.id.toString())
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event2.id.toString())
    }

    @Test
    fun shouldReturnThirdItemInDatabase_whenPageZeroAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "0")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", event2.id.toString())
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsOnly(event3.id.toString())
    }

    @Test
    fun shouldReturnEmptyPage_whenPageTwoAndSizeOneAndLastReceivedIDProvided() {
        val response =
            given()
                .queryParam("page", "2")
                .queryParam("size", "1")
                .queryParam("lastReceivedID", eventOtherType2.id.toString())
                .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).isEmpty()
    }

    @Test
    fun shouldReturnEventById_whenEventExists() {
        val response = given()
            .`when`()
            .get("/v2/credentials-events/${event1.id}")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(200)

        val returnedEvent = response.jsonPath().getObject("", JWTDecodedCloudEventDTO::class.java)
        assertThat(returnedEvent.id).isEqualTo(event1.id)

        assertThat(returnedEvent.type).isEqualTo(event1.content.type)
    }

    @Test
    fun shouldReturnEventWithCreationDate_whenEventExists() {
        val response = given()
            .`when`()
            .get("/v2/credentials-events/${event1.id}")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(200)

        val returnedEvent = response.jsonPath().getObject("", JWTDecodedCloudEventDTO::class.java)
        assertThat(returnedEvent.cesReceivedDate).isNotNull()
    }

    @Test
    fun shouldReturnNotFound_whenEventDoesNotExist() {
        val nonExistentId = UUID.randomUUID()
        val response = given()
            .`when`()
            .get("/v2/credentials-events/$nonExistentId")
            .andReturn()

        assertThat(response.statusCode).isEqualTo(404)
    }

    @Test
    fun shouldReturnCorrectContentType_whenSingleEvent() {
        val response = given()
            .`when`()
            .get("/v2/credentials-events/${event1.id}")
            .andReturn()

        val normalizedType = response.contentType.replace("; ", ";")
        assertThat(normalizedType).isEqualTo("application/cloudevents+json;charset=UTF-8")
    }

    @Test
    fun shouldReturnCorrectContentType_whenMultipleEvents() {
        val response = given()
            .`when`()
            .get("/v2/credentials-events")
            .andReturn()

        val normalizedType = response.contentType.replace("; ", ";")
        assertThat(normalizedType).isEqualTo("application/cloudevents-batch+json;charset=UTF-8")
    }

    @Test
    fun shouldReturnComplianceItemsInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "eu.gaia-x.credential")
            .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            event1.id.toString(),
            event2.id.toString(),
            event3.id.toString(),
        )
    }

    @Test
    fun shouldReturnOtherTypeItemInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .queryParam("page", "0")
            .queryParam("size", "1")
            .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType.id.toString(),
        )
    }

    @Test
    fun shouldReturnOtherTypeItemsInDatabase_whenTypeProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType.id.toString(),
            eventOtherType2.id.toString()
        )
    }


    @Test
    fun shouldReturnNextOtherTypeItemsInDatabase_whenTypeProvidedAndLastReceivedIDProvided() {
        val response = given()
            .queryParam("type", "edu.example.credential")
            .queryParam("lastReceivedID", eventOtherType.id)
            .`when`().get("/v2/credentials-events").andReturn()
        assertThat(response.statusCode).isEqualTo(200)
        val ids = response.jsonPath().get<List<String>>("id")
        assertThat(ids).containsExactly(
            eventOtherType2.id.toString()
        )
    }


    companion object {
        lateinit var event1: JWTEvent
        lateinit var event2: JWTEvent
        lateinit var event3: JWTEvent
        lateinit var eventOtherType: JWTEvent
        lateinit var eventOtherType2: JWTEvent

        @BeforeAll
        @JvmStatic
        @Transactional
        fun setUp() {
            event1 = JWTEvent()
            event1.id = UUID.fromString("e04ef77f-83f2-4dcc-9a14-361029534d7a")
            event1.creationDate = ZonedDateTime.now().minusSeconds(1)
            event1.content = JWTCloudEventDTO(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", "eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJpc3N1ZXIiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tIiwidmFsaWRVbnRpbCI6IjIwMjQtMTAtMjlUMTQ6NDE6MDguNjk4KzAwOjAwIn0.o7TxxXu5IeeY9jEakMNG8KCtAinCl5kL2YNqB9h7V6MmhRjW_dWVY4RWCMgEHxnzHKW_4wXMiNbhdG2uGGFspw"
            )

            event2 = JWTEvent()
            event2.id = UUID.fromString("bf215590-bb87-4093-88df-893b7eb6c797")
            event2.creationDate = ZonedDateTime.now()
            event2.content = JWTCloudEventDTO(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", "eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJpc3N1ZXIiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tIiwidmFsaWRVbnRpbCI6IjIwMjQtMTAtMjlUMTQ6NDE6MDguNjk4KzAwOjAwIn0.o7TxxXu5IeeY9jEakMNG8KCtAinCl5kL2YNqB9h7V6MmhRjW_dWVY4RWCMgEHxnzHKW_4wXMiNbhdG2uGGFspw"
            )

            // event3 is created at the same time as event2. Its uuid is greater so it is presented after event2
            event3 = JWTEvent()
            event3.id = UUID.fromString("d93e1336-1a8f-4f8b-9b9a-2cd546ad9daa")
            event3.creationDate = event2.creationDate
            event3.content = JWTCloudEventDTO(
                "1.0", "eu.gaia-x.credential", "", null, null, ZonedDateTime.now(), "application/json", "eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJpc3N1ZXIiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tIiwidmFsaWRVbnRpbCI6IjIwMjQtMTAtMjlUMTQ6NDE6MDguNjk4KzAwOjAwIn0.o7TxxXu5IeeY9jEakMNG8KCtAinCl5kL2YNqB9h7V6MmhRjW_dWVY4RWCMgEHxnzHKW_4wXMiNbhdG2uGGFspw"
            )

            // eventOtherType is of type "edu.example.credential"
            eventOtherType = JWTEvent()
            eventOtherType.id = UUID.fromString("aaae9227-0cde-daca-9b9a-2cd546ad9cca")
            eventOtherType.creationDate = ZonedDateTime.now().plusMinutes(5)
            eventOtherType.content = JWTCloudEventDTO(
                "1.0", "edu.example.credential", "", null, null, ZonedDateTime.now(), "application/json", "eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJpc3N1ZXIiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tIiwidmFsaWRVbnRpbCI6IjIwMjQtMTAtMjlUMTQ6NDE6MDguNjk4KzAwOjAwIn0.o7TxxXu5IeeY9jEakMNG8KCtAinCl5kL2YNqB9h7V6MmhRjW_dWVY4RWCMgEHxnzHKW_4wXMiNbhdG2uGGFspw"
            )

            eventOtherType2 = JWTEvent()
            eventOtherType2.id = UUID.fromString("0825ba37-e9c2-416a-b1e5-dcd037f94d5a")
            eventOtherType2.creationDate = ZonedDateTime.now().plusMinutes(5)
            eventOtherType2.content = JWTCloudEventDTO(
                "1.0", "edu.example.credential", "", null, null, ZonedDateTime.now(), "application/json", "eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJpc3N1ZXIiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tIiwidmFsaWRVbnRpbCI6IjIwMjQtMTAtMjlUMTQ6NDE6MDguNjk4KzAwOjAwIn0.o7TxxXu5IeeY9jEakMNG8KCtAinCl5kL2YNqB9h7V6MmhRjW_dWVY4RWCMgEHxnzHKW_4wXMiNbhdG2uGGFspw"
            )

            JWTEvent.persist(event1, event2, event3, eventOtherType, eventOtherType2)
            JWTEvent.flush()
        }

        @AfterAll
        @Transactional
        @JvmStatic
        fun tearDown() {
            JWTEvent.deleteAll()
            JWTEvent.flush()
        }
    }
}