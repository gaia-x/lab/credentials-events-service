package eu.gaiax.ces.v2.service

import com.fasterxml.jackson.databind.ObjectMapper
import eu.gaiax.ces.common.exception.InvalidContentException
import eu.gaiax.ces.common.exception.InvalidIssuerException
import eu.gaiax.ces.common.exception.InvalidSignatureException
import eu.gaiax.ces.common.exception.InvalidTypeException
import eu.gaiax.ces.common.service.TrustedIssuersService
import io.quarkus.test.InjectMock
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.annotation.PostConstruct
import jakarta.inject.Inject
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when`

@QuarkusTest
@TestTransaction
class JWTCredentialEventServiceTest {
    lateinit var JWTCredentialTest: JWTCredentialTest

    @Inject
    lateinit var jwtCredentialEventService: JWTCredentialEventService

    @InjectMock
    lateinit var jwtSignatureService: JWTSignatureService

    @InjectMock
    lateinit var trustedIssuersService: TrustedIssuersService

    @Inject
    lateinit var objMapper: ObjectMapper


    @PostConstruct
    fun setUp() {
        JWTCredentialTest = JWTCredentialTest(this.objMapper)
    }


    @Test
    fun shouldThrowAnInvalidSignatureException_whenSignatureIsInvalid() {
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(false)
        Assertions.assertThatThrownBy {
            jwtCredentialEventService.handleCredentialsEvent(
                JWTCredentialTest.unproperlySignedJWTCloudEventDTO
            )
        }.isInstanceOf(InvalidSignatureException::class.java)
    }

    @Test
    fun shouldThrowAnInvalidContentException_whenDataIsNotAComplianceCredential() {
        Assertions.assertThatThrownBy {
            jwtCredentialEventService.handleCredentialsEvent(
                JWTCredentialTest.emptyJWTCloudEventDTO
            )
        }.isInstanceOf(InvalidContentException::class.java)
    }

    @Test
    fun shouldThrowAnInvalidIssuerException_whenDataIssuerIsNotInTrustedList() {
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(false)
        Assertions.assertThatThrownBy {
            jwtCredentialEventService.handleCredentialsEvent(
                JWTCredentialTest.invalidIssuerSignedJWTCloudEventDTO
            )
        }.isInstanceOf(InvalidIssuerException::class.java)
    }


    @Test
    fun shouldDoNothing_whenSignatureIsValid() {
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        assertDoesNotThrow(fun() {
            jwtCredentialEventService.handleCredentialsEvent(JWTCredentialTest.signedJWTCloudEventDTO)
        })
    }

    @Test
    @DisplayName("should throw an error when event type is eu.gaia-x.credential and no subject of type gx:compliance")
    fun shouldThrowTypeException_whenTypeIsGxCredentialAndAnySubjectIsNotCompliance() {
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(true)
        `when`(trustedIssuersService.isIssuerAllowed(anyString())).thenReturn(true)
        assertThrows<InvalidTypeException> {
            jwtCredentialEventService.handleCredentialsEvent(JWTCredentialTest.invalidJWTCloudEventDTOOtherType)
        }
    }

    @Test
    @DisplayName("should throw an error when event type is not eu.gaia-x.credential and VC contains an ID and an another field")
    fun shouldThrowTypeException_whenTypeIsNotGxComplianceAndContainsSomethingElseThanID() {
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(true)
        assertThrows<InvalidTypeException> {
            jwtCredentialEventService.handleCredentialsEvent(JWTCredentialTest.nonGaiaXEventContainingMoreInfoThanId)
        }
    }

    @Test
    @DisplayName("should accept when event type is not eu.gaia-x.credential and VC contains only an ID")
    fun shouldDoNothing_whenTypeIsNotGxComplianceAndContainsOnlyID() {
        `when`(jwtSignatureService.isSignatureValid(anyString())).thenReturn(true)
        assertDoesNotThrow {
            jwtCredentialEventService.handleCredentialsEvent(JWTCredentialTest.nonGaiaXEventContainingOnlyID)
        }
    }
}