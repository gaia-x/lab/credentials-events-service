package eu.gaiax.ces.v2.service

import com.fasterxml.jackson.databind.ObjectMapper
import eu.gaiax.ces.common.exception.InvalidSignatureException
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.annotation.PostConstruct
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import uniresolver.ResolutionException
import uniresolver.UniResolver
import uniresolver.result.ResolveDataModelResult

@QuarkusTest
class JWTSignatureServiceTest {
    @Inject
    lateinit var objMapper: ObjectMapper
    lateinit var JWTCredentialTest: JWTCredentialTest

    @InjectMock
    lateinit var uniResolver: UniResolver

    @Inject
    lateinit var jwtSignatureService: JWTSignatureService

    @PostConstruct
    fun setUp() {
        JWTCredentialTest = JWTCredentialTest(this.objMapper)
    }


    @Test
    fun shouldReturnTrue_whenSignatureIsValid() {
        val result = ResolveDataModelResult.build()
        result.didDocument = JWTCredentialTest.complianceDID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThat(jwtSignatureService.isSignatureValid(JWTCredentialTest.signedJWTCloudEventDTO.dataBase64)).isTrue()
    }

    @Test
    fun shouldReturnFalse_whenSignatureIsInvalid() {
        val result = ResolveDataModelResult.build()
        result.didDocument = JWTCredentialTest.complianceDID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThat(jwtSignatureService.isSignatureValid(JWTCredentialTest.unproperlySignedJWTCloudEventDTO.dataBase64)).isFalse()
    }

    @Test
    fun shouldThrowInvalidSignatureException_whenSignatureIsMissing() {
        val result = ResolveDataModelResult.build()
        result.didDocument = JWTCredentialTest.complianceDID
        `when`(uniResolver.resolve(Mockito.anyString())).thenReturn(result)

        assertThrows<InvalidSignatureException> {
            jwtSignatureService.isSignatureValid(JWTCredentialTest.missingSignatureJWTCloudEventDTO.dataBase64)
        }
    }

    @Test
    fun shouldReturnFalse_whenSignatureVerificationMethodNotInDID() {
        val result = ResolveDataModelResult.build()
        `when`(uniResolver.resolve(anyString())).thenReturn(result)
        assertThat(jwtSignatureService.isSignatureValid(JWTCredentialTest.signedJWTCloudEventDTO.dataBase64)).isFalse()
    }

    @Test
    fun shouldReturnFalse_whenDIDIsNotReachable() {
        `when`(uniResolver.resolve(anyString())).thenThrow(ResolutionException("Unable to reach DID"))
        assertThat(jwtSignatureService.isSignatureValid(JWTCredentialTest.signedJWTCloudEventDTO.dataBase64)).isFalse()
    }
}