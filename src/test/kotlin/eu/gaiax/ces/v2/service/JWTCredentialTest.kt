package eu.gaiax.ces.v2.service

import com.fasterxml.jackson.databind.ObjectMapper
import foundation.identity.did.DIDDocument

class JWTCredentialTest(objectMapper: ObjectMapper) {
    var unproperlySignedJWTCloudEventDTO: JWTCloudEventDTO
    var invalidIssuerSignedJWTCloudEventDTO: JWTCloudEventDTO
    var signedJWTCloudEventDTO: JWTCloudEventDTO
    var emptyJWTCloudEventDTO: JWTCloudEventDTO
    var missingSignatureJWTCloudEventDTO: JWTCloudEventDTO
    var invalidJWTCloudEventDTOOtherType: JWTCloudEventDTO
    var nonGaiaXEventContainingMoreInfoThanId: JWTCloudEventDTO
    var nonGaiaXEventContainingOnlyID: JWTCloudEventDTO
    var complianceDID: DIDDocument

    init {
        val resource = this::class.java.getResourceAsStream("/v2/unproperly_signed_compliance_cloud_event.json")
        resource.use {
            unproperlySignedJWTCloudEventDTO =
                objectMapper.readValue(
                    resource, JWTCloudEventDTO::class.java
                )
        }
        val signedComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v2/signed_compliance_cloud_event.json")
        signedComplianceCloudEventResource.use {
            signedJWTCloudEventDTO =
                objectMapper.readValue(
                    signedComplianceCloudEventResource, JWTCloudEventDTO::class.java
                )
        }
        val emptyComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v2/empty_compliance_cloud_event.json")
        emptyComplianceCloudEventResource.use {
            emptyJWTCloudEventDTO =
                objectMapper.readValue(
                    emptyComplianceCloudEventResource, JWTCloudEventDTO::class.java
                )
        }
        val missingSignatureComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v2/missing_signature_compliance_cloud_event.json")
        missingSignatureComplianceCloudEventResource.use {
            missingSignatureJWTCloudEventDTO =
                objectMapper.readValue(
                    missingSignatureComplianceCloudEventResource, JWTCloudEventDTO::class.java
                )
        }
        val invalidIssuerSignedComplianceCloudEventResource =
            this::class.java.getResourceAsStream("/v2/invalid_issuer_signed_compliance_cloud_event.json")
        invalidIssuerSignedComplianceCloudEventResource.use {
            invalidIssuerSignedJWTCloudEventDTO =
                objectMapper.readValue(
                    invalidIssuerSignedComplianceCloudEventResource,
                    JWTCloudEventDTO::class.java
                )
        }
        val complianceDIDResource = this::class.java.getResourceAsStream("/v2/did/compliance_did.json")
        complianceDIDResource.use {
            complianceDID = DIDDocument.fromJson(
                String(complianceDIDResource!!.readAllBytes())
            )
        }
        val invalidSignedComplianceCloudEventOtherTypeResource =
            this::class.java.getResourceAsStream("/v2/invalid_compliance_cloud_event_other_type.json")
        invalidSignedComplianceCloudEventOtherTypeResource.use {
            invalidJWTCloudEventDTOOtherType =
                objectMapper.readValue(
                    invalidSignedComplianceCloudEventOtherTypeResource,
                    JWTCloudEventDTO::class.java
                )
        }
        val nonGaiaXEventContainingMoreThanInfoResource =
            this::class.java.getResourceAsStream("/v2/non_gaiax_event_containing_more_than_id.json")
        nonGaiaXEventContainingMoreThanInfoResource.use {
            nonGaiaXEventContainingMoreInfoThanId =
                objectMapper.readValue(
                    nonGaiaXEventContainingMoreThanInfoResource, JWTCloudEventDTO::class.java
                )
        }
        val nonGaiaXEventContainingOnlyIDResource =
            this::class.java.getResourceAsStream("/v2/non_gaiax_event_containing_only_id.json")
        nonGaiaXEventContainingOnlyIDResource.use {
            nonGaiaXEventContainingOnlyID =
                objectMapper.readValue(
                    nonGaiaXEventContainingOnlyIDResource, JWTCloudEventDTO::class.java
                )
        }
    }

}