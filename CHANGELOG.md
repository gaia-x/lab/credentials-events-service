## [1.3.1](https://gitlab.com/gaia-x/lab/credentials-events-service/compare/v1.3.0...v1.3.1) (2024-03-21)


### Bug Fixes

* query invalid when type and received id provided ([a55d6f1](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/a55d6f1c1334f8278bea4f157fc4529ab4c53b47))

# [1.3.0](https://gitlab.com/gaia-x/lab/credentials-events-service/compare/v1.2.0...v1.3.0) (2024-02-21)


### Features

* add kyverno policy ([dc595a0](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/dc595a0bc59ff507760ad411b82a95d678794bee))
* **LAB-465:** custom cloud events id sharing ([dcebafe](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/dcebafe5a7fd338090ca251a30c15acff83bfc67))
* **LAB-512:** Remove kafka backend ([eeaeade](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/eeaeade8aa15bbfc959d3704f25a6e771a94b9be))

# [1.2.0](https://gitlab.com/gaia-x/lab/credentials-events-service/compare/v1.1.0...v1.2.0) (2023-10-18)


### Bug Fixes

* **#6:** credentials events content type ([7464ef1](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/7464ef1ebeb7835fb927d41a40b1e98fcf1698a5)), closes [#6](https://gitlab.com/gaia-x/lab/credentials-events-service/issues/6)
* add exception mappers for unhandled exception ([04bc7ad](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/04bc7ad1e8af13a0a5f1332adfecca61b0e359e4))


### Features

* **LAB-388:** use local documents for known contexts ([1e0c79a](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/1e0c79a546530c8d5b385f13152127cf5a6b9eb5))

# [1.1.0](https://gitlab.com/gaia-x/lab/credentials-events-service/compare/v1.0.0...v1.1.0) (2023-09-28)


### Bug Fixes

* deterministically order concurrent events ([b6aa7b6](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/b6aa7b66bc0270ed5d07c9034c26a88dca76c801))


### Features

* add get endpoint by id for credentials event ([b9db2bc](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/b9db2bcf7aaa146fd5f7ece057b53b580a212132))
* **LAB-375:** expose event creation date ([2a11b08](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/2a11b0882400fef78dfc43f1a3f2f15b05a2af82))

# 1.0.0 (2023-08-28)


### Features

* initialize credentials-events-service ([8866808](https://gitlab.com/gaia-x/lab/credentials-events-service/commit/8866808334456ae6f17b587b433af63f7bd8b94e))
